/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of SDK for AQQ IM                                         *
 *                                                                             *
 * SDK for AQQ IM is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * SDK is distributed in the hope that it will be useful,                      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#pragma once
#include <map>
#include <vcl.h>

class CIconManager {
private:

	CIconManager() {};
	static CIconManager *Instance;
	typedef std::map<UnicodeString, int>::iterator Icons_it;
	std::map<UnicodeString, int> Icons;

public:
	~CIconManager();
	static CIconManager * instance();
	void LoadIcon(UnicodeString Name, UnicodeString Filename);
	void DestroyIcon(UnicodeString Name);
	void ReplaceIcon(UnicodeString Name, UnicodeString Filename);
	int GetIconByName(UnicodeString Name);
	UnicodeString GetIconPathByName(UnicodeString Name);
	void DestroyAllIcons();
};
