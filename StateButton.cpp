/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of SDK for AQQ IM                                         *
 *                                                                             *
 * SDK for AQQ IM is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * SDK is distributed in the hope that it will be useful,                      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#include "StateButton.h"
#include "AQQ.h"
#include "Synchronizer.h"

CStateButton::CStateButton()
{
	this->Action = "";
	this->Name = "";
	this->Caption = "";
	this->Position = 0;
	this->IconID = -1;
	this->Service = "";
	this->PopupName = "";
	this->PopupPosition = 0;
	this->GroupIndex = 0;
	this->Grouped = false;
	this->AutoCheck = false;
	this->Checked = false;
	this->Handle = 0;
	this->Shortcut = "";
	this->Hint = "";
	this->PositionAfter = "";

	PluginAction = new TPluginAction;
	BlinkTimer = new TTimer(NULL);
	Synchronizer = new CSynchronizer();

	Timeout = 0;
	Elapsed = 0;
	BlinkActualIcon = 0;
	UseTimeout = false;
	Stop = false;
}

CStateButton::~CStateButton()
{
	this->StopBlink();
	this->DestroyButton();
	delete Synchronizer;
	delete BlinkTimer;
	delete PluginAction;
}

PPluginAction CStateButton::AQQFormat()
{
	ZeroMemory(this->PluginAction, sizeof(TPluginAction));
	this->PluginAction->cbSize = sizeof(TPluginAction);
	this->PluginAction->Action = this->Action.c_str();
	this->PluginAction->pszName = this->Name.c_str();
	this->PluginAction->pszCaption = this->Caption.c_str();
	this->PluginAction->Position = this->Position;
	this->PluginAction->IconIndex = this->IconID;
	this->PluginAction->pszService = this->Service.c_str();
	this->PluginAction->pszPopupName = this->PopupName.c_str();
	this->PluginAction->PopupPosition = this->PopupPosition;
	this->PluginAction->GroupIndex = this->GroupIndex;
	this->PluginAction->Grouped = this->Grouped;
	this->PluginAction->AutoCheck = this->AutoCheck;
	this->PluginAction->Checked = this->Checked;
	this->PluginAction->Handle = this->Handle;
	this->PluginAction->ShortCut = this->Shortcut.c_str();
	this->PluginAction->Hint = this->Hint.c_str();
	this->PluginAction->PositionAfter = this->PositionAfter.c_str();

	return this->PluginAction;
}

void CStateButton::SDKFormat(PPluginAction PluginAction)
{
	this->Action = PluginAction->Action;
	this->Name = PluginAction->pszName;
	this->Caption = PluginAction->pszCaption;
	this->Position = PluginAction->Position;
	this->IconID = PluginAction->IconIndex;
	this->Service = PluginAction->pszService;
	this->PopupName = PluginAction->pszPopupName;
	this->PopupPosition = PluginAction->PopupPosition;
	this->GroupIndex = PluginAction->GroupIndex;
	this->Grouped = PluginAction->Grouped;
	this->AutoCheck = PluginAction->AutoCheck;
	this->Checked = PluginAction->Checked;
	this->Handle = PluginAction->Handle;
	this->Shortcut = PluginAction->ShortCut;
	this->Hint = PluginAction->Hint;
	this->PositionAfter = PluginAction->PositionAfter;
}

void CStateButton::CreateButton()
{
	AQQ::Controls::CreateButton(this->AQQFormat());
}

void CStateButton::CreateButton(UnicodeString Name, int Position, int Icon, UnicodeString ServiceFunction, UnicodeString PopupName)
{
	this->Name = Name;
	this->Position = Position;
	this->IconID = Icon;
	this->Service = ServiceFunction;
	this->PopupName = PopupName;
	AQQ::Controls::CreateButton(this->AQQFormat());
}

void CStateButton::CreateButton(UnicodeString Name, UnicodeString PositionAfter, int Icon, UnicodeString ServiceFunction, UnicodeString PopupName)
{
	this->Name = Name;
	this->PositionAfter = PositionAfter;
	this->IconID = Icon;
	this->Service = ServiceFunction;
	this->PopupName = PopupName;
	AQQ::Controls::CreateButton(this->AQQFormat());
}

void CStateButton::DestroyButton()
{
	AQQ::Controls::DestroyButton(this->AQQFormat());
}

void CStateButton::UpdateNS()
{
	AQQ::Controls::UpdateButton(this->AQQFormat());
}

void CStateButton::Update()
{
	Synchronizer->UpdateStateButton(this);
}

void CStateButton::Blink()
{
	Stop = false;
	UseTimeout = false;
	BlinkTimer->Interval = 1000;
	BlinkTimer->OnTimer = this->BlinkTimerEvent;
	BlinkActualIcon = BlinkStartIcon;
	this->IconID = BlinkActualIcon;
	this->Update();
	BlinkTimer->Enabled = true;
}

void CStateButton::Blink(int Timeout)
{
	Stop = false;
	UseTimeout = true;
	this->Timeout = Timeout;
	this->Elapsed = 0;
	Blink();
}

void CStateButton::StopBlink()
{
	Stop = true;
	UseTimeout = false;
	BlinkTimer->Enabled = false;
	this->IconID = BlinkStopIcon;
	this->Update();
}

void __fastcall CStateButton::BlinkTimerEvent(TObject *Sender)
{
	if (UseTimeout)
	{
    	if (Elapsed == Timeout)
		{
			StopBlink();
			return;
		}
		else
			++Elapsed;
	}

	if (Stop)
		return;

	if (BlinkActualIcon == BlinkStartIcon)
		BlinkActualIcon = BlinkStopIcon;
	else
		BlinkActualIcon = BlinkStartIcon;
	this->IconID = BlinkActualIcon;
	this->Update();
}
