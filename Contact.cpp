/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of SDK for AQQ IM                                         *
 *                                                                             *
 * SDK for AQQ IM is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * SDK is distributed in the hope that it will be useful,                      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#include "Contact.h"
#include "AQQ.h"

CContact::CContact()
{
	this->JID = "";
	this->Nick = "";
	this->Resource = "";
	this->Groups = "";
	this->Status.State = 0;
	this->Status.Description = "";
	this->Temporary = false;
	this->FromPlugin = false;
	this->UserIDx = 0;
	this->Subscription = SUB_BOTH;
	this->Chat = false;

	PluginContact = new TPluginContact;
	ContactInfo = new CContactInfo(this);
	Avatar = new CAvatar(this);
	Settings = new CSettings("Contacts.dat");
	this->AccountName = "Not specifed";
}

CContact::CContact(UnicodeString AccountName)
{
	this->JID = "";
	this->Nick = "";
	this->Resource = "";
	this->Groups = "";
	this->Status.State = 0;
	this->Status.Description = "";
	this->Temporary = false;
	this->FromPlugin = false;
	this->UserIDx = 0;
	this->Subscription = SUB_BOTH;
	this->Chat = false;

	PluginContact = new TPluginContact;
	ContactInfo = new CContactInfo(this);
	Avatar = new CAvatar(this);
	Settings = new CSettings("Contacts.dat");
	this->AccountName = AccountName;
}

CContact::CContact(UnicodeString AccountName, UnicodeString JID)
{
	this->JID = "";
	this->Nick = "";
	this->Resource = "";
	this->Groups = "";
	this->Status.State = 0;
	this->Status.Description = "";
	this->Temporary = false;
	this->FromPlugin = false;
	this->UserIDx = 0;
	this->Subscription = SUB_BOTH;
	this->Chat = false;

	PluginContact = new TPluginContact;
	ContactInfo = new CContactInfo(this);
	Avatar = new CAvatar(this);
	this->JID = JID;
	Settings = new CSettings("Contacts.dat");
	this->AccountName = AccountName;
}

CContact::~CContact(){
	delete PluginContact;
	delete ContactInfo;
	delete Avatar;
	delete Settings;
}

PPluginContact CContact::AQQFormat(){
	ZeroMemory(this->PluginContact,sizeof(TPluginContact));
	this->PluginContact->cbSize = sizeof(TPluginContact);
	this->PluginContact->JID = this->JID.c_str();
	this->PluginContact->Nick = this->Nick.c_str();
	this->PluginContact->Resource = this->Resource.c_str();
	this->PluginContact->Groups = this->Groups.c_str();
	this->PluginContact->State = this->Status.State;
	this->PluginContact->Status = this->Status.Description.c_str();
	this->PluginContact->Temporary = this->Temporary;
	this->PluginContact->FromPlugin = this->FromPlugin;
	this->PluginContact->UserIdx = this->UserIDx;
	this->PluginContact->Subscription = this->Subscription;
	this->PluginContact->IsChat = this->Chat;
	return this->PluginContact;
}

void CContact::SDKFormat(PPluginContact PluginContact){
	this->JID = PluginContact->JID;
	this->Nick = PluginContact->Nick;
	this->Resource = PluginContact->Resource;
	this->Groups = PluginContact->Groups;
	this->Status.State = PluginContact->State;
	this->Status.Description = PluginContact->Status;
	this->Temporary = PluginContact->Temporary;
	this->FromPlugin = PluginContact->FromPlugin;
	this->UserIDx = PluginContact->UserIdx;
	this->Subscription = PluginContact->Subscription;
	this->Chat = PluginContact->IsChat;
}

void CContact::Create(){
	AQQ::Contacts::Create(this->AQQFormat());
	ContactInfo->SetSimpleInfo();
}

void CContact::Delete(){
	AQQ::Contacts::Delete(this->AQQFormat());
}

void CContact::Update(){
	AQQ::Contacts::Update(this->AQQFormat());
}

void CContact::Ban(){
	if (!this->HaveBan())
		AQQ::Contacts::AddBan(this->JID);
}

bool CContact::HaveBan(){
	return AQQ::Contacts::HaveBan(this->JID);
}

void CContact::RemoveBan(){
	if (this->HaveBan())
		AQQ::Contacts::RemoveBan(this->JID);
}

CStatus CContact::GetStatus()
{
	return Status;
}

void CContact::SetStatus(int State, UnicodeString Description)
{
	this->Status.State = State;
	this->Status.Description = Description;
	this->Update();
}

void CContact::SaveSettings()
{
	Settings->AddString(JID, "JID", JID);
	Settings->AddString(JID, "Nick", Nick);
	Settings->AddString(JID, "Resource", Resource);
	Settings->AddString(JID, "Groups", Groups);
	Settings->AddString(JID, "Description", Status.Description);
	Settings->AddBool(JID, "Temporary", Temporary);
	Settings->AddBool(JID, "FromPlugin", FromPlugin);
	Settings->AddBool(JID, "Chat", Chat);
	Settings->AddInt(JID, "Subscription", Subscription);
	Settings->AddString(JID, "ParentAccount", AccountName);
}

void CContact::LoadSettings()
{
	Nick = Settings->GetString(JID, "Nick");
	Resource = Settings->GetString(JID, "Resource");
	Groups = Settings->GetString(JID, "Groups");
	Status.Description = Settings->GetString(JID, "Description");
	Temporary = Settings->GetBool(JID, "Temporary");
	FromPlugin = Settings->GetBool(JID, "FromPlugin");
	Chat = Settings->GetBool(JID, "Chat");
	Subscription = Settings->GetInt(JID, "Subscription");
}

void CContact::EraseSettings()
{
	Settings->EraseSection(JID);
}
