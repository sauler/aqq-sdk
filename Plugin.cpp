/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of SDK for AQQ IM                                         *
 *                                                                             *
 * SDK for AQQ IM is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * SDK is distributed in the hope that it will be useful,                      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#include "Plugin.h"
#include "PluginLink.h"

CPlugin* CPlugin::Instance = 0;

CPlugin* CPlugin::instance()
{
	if (Instance == 0)
		Instance = new CPlugin();
	return Instance;
}

UnicodeString CPlugin::GetPluginPath()
{
	return CPluginLink::instance()->GetLink().Path;
}

UnicodeString CPlugin::GetPluginName()
{
	UnicodeString Path = CPluginLink::instance()->GetLink().Path;
	UnicodeString Filename = ExtractFileName(Path);
	UnicodeString PluginName = StringReplace(Filename, ".dll", "", TReplaceFlags() << rfReplaceAll);
	return PluginName;
}
