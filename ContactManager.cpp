/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of SDK for AQQ IM                                         *
 *                                                                             *
 * SDK for AQQ IM is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * SDK is distributed in the hope that it will be useful,                      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#include "ContactManager.h"
#include "AQQ.h"


CContactManager::CContactManager()
{

}

CContactManager::~CContactManager()
{
	this->DeleteAllContacts();
}

void CContactManager::AddContact(CContact* Contact)
{
	if (this->GetContactByJID(Contact->JID) == NULL)
	{
    	Contacts[Contact->JID] = Contact;
		LastAddedContact = Contact->JID;
	}
}

void CContactManager::RemoveContact(UnicodeString JID)
{
	CContact* Contact = Contacts[JID];
	Contact->Delete();
	delete Contact;
	Contacts.erase(JID);
}

CContact* CContactManager::GetContactByJID(UnicodeString JID)
{
	return Contacts[JID];
}

CContact* CContactManager::GetLastAddedContact()
{
	return GetContactByJID(LastAddedContact);
}

void CContactManager::DeleteAllContacts()
{
	for (Contacts_it it=Contacts.begin(); it!=Contacts.end(); ++it)
		RemoveContact(it->second->JID);
}


