/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of SDK for AQQ IM                                         *
 *                                                                             *
 * SDK for AQQ IM is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * SDK is distributed in the hope that it will be useful,                      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#include "Popup.h"
#include "AQQ.h"

CPopup::CPopup()
{
	this->Name = "";

	Popup = new TPluginAction;
	PopupItemsManager = new CPopupItemsManager();
}

CPopup::CPopup(UnicodeString Name)
{
	Popup = new TPluginAction;
	PopupItemsManager = new CPopupItemsManager();
	this->Name = Name;
	this->Create();
}

CPopup::~CPopup()
{
	this->Destroy();
	delete PopupItemsManager;
	delete Popup;
}

void CPopup::Create()
{
	ZeroMemory(this->Popup, sizeof(TPluginAction));
	this->Popup->cbSize = sizeof(TPluginAction);
	this->Popup->pszName = this->Name.c_str();
	AQQ::Controls::CreatePopupMenu(this->Popup);
}

void CPopup::Destroy()
{
	AQQ::Controls::DestroyPopupMenu(this->Popup);
}

void CPopup::AddItem(CPopupMenuItem* Item)
{
	Item->PopupName = this->Name;
    PopupItemsManager->AddItem(Item);
}


void CPopup::RemoveItem(UnicodeString Name)
{
	PopupItemsManager->RemoveItem(Name);
}

CPopupMenuItem* CPopup::GetItem(UnicodeString Name)
{
	PopupItemsManager->GetItemByName(Name);
}

CPopupMenuItem* CPopup::GetLastAddedItem()
{
	PopupItemsManager->GetLastAddedItem();
}
