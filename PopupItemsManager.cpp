/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of SDK for AQQ IM                                         *
 *                                                                             *
 * SDK for AQQ IM is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * SDK is distributed in the hope that it will be useful,                      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#include "PopupItemsManager.h"

CPopupItemsManager::CPopupItemsManager()
{

}

CPopupItemsManager::~CPopupItemsManager()
{
	this->DestroyAllItems();
}

void CPopupItemsManager::AddItem(CPopupMenuItem* Item)
{
	Items[Item->Name] = Item;
	LastAddedItem = Item->Name;
	Item->Create();
}

void CPopupItemsManager::RemoveItem(UnicodeString Name)
{
	CPopupMenuItem* Item = Items[Name];
	delete Item;
	Items.erase(Name);
}
CPopupMenuItem* CPopupItemsManager::GetItemByName(UnicodeString Name)
{
	return Items[Name];
}

CPopupMenuItem* CPopupItemsManager::GetItemByCaption(UnicodeString Caption)
{
	for (Items_it it = Items.begin();it != Items.end();++it)
		if (it->second->Caption == Caption)
			return it->second;
		else
			return NULL;
}

CPopupMenuItem* CPopupItemsManager::GetLastAddedItem()
{
	return GetItemByName(LastAddedItem);
}

void CPopupItemsManager::DestroyAllItems()
{
	for (Items_it it = Items.begin();it != Items.end();++it)
    	RemoveItem(it->second->Name);
}
