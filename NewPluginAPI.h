#pragma once
#include <string>
#include <Windows.h>


namespace AQQAPI {
	namespace Notifications {
		namespace System {
			const UnicodeString ACCOUNT_RUNEVENT = "AQQ/System/Account/RunEvent";
			const UnicodeString ADDONBROWSER_URL = "AQQ/System/AddonBrowser/URL";
			const UnicodeString ADDONINSTALLED = "AQQ/System/AddonInstalled";
			const UnicodeString AUTOMATION_AUTOAWAY_OFF = "AQQ/System/Automation/AutoAway/Off";
			const UnicodeString AUTOMATION_AUTOAWAY_ON = "AQQ/System/Automation/AutoAway/On";
			const UnicodeString AUTOMATION_AUTOSECURE = "AQQ/System/Automation/AutoSecure";
			const UnicodeString AUTOSECURE_OFF = "AQQ/System/AutoSecure/Off";
			const UnicodeString AUTOSECURE_ON = "AQQ/System/AutoSecure/On";
			const UnicodeString BEFOREUNLOAD = "AQQ/System/BeforeUnload";
			const UnicodeString CLIPBOARD_JID = "AQQ/System/Clipboard/JID";
			const UnicodeString COLORCHANGE = "AQQ/System/ColorChange";
			const UnicodeString COLORCHANGEV2 = "AQQ/System/ColorChangeV2";
			const UnicodeString CONNECTION_STATE = "AQQ/System/Connection/State";
			const UnicodeString CURRENTSONG = "AQQ/System/CurrentSong";
			const UnicodeString DISCONNECT = "AQQ/System/Disconnect";
			const UnicodeString FORCESTATUS = "AQQ/System/ForceStatus";
			const UnicodeString GETACCOUNTINFO = "AQQ/System/GetAccountInfo";
			const UnicodeString GETAGENTS = "AQQ/System/GetAgents";
			const UnicodeString GETCURRENTSHOWTYPE_PATH = "AQQ/System/GetCurrentShowType/Path";
			const UnicodeString GETCURRENTUID = "AQQ/System/GetCurrentUID";
			const UnicodeString GETIMPEXP = "AQQ/System/GetImpExp";
			const UnicodeString ISAGENTENABLED = "AQQ/System/IsAgentEnabled";
			const UnicodeString LANGCODE_CHANGED = "AQQ/System/LangCodeChanged";
			const UnicodeString LASTURL = "AQQ/System/LastURL";
			const UnicodeString MAXMSGLENGTH = "AQQ/System/MaxMsgLength";
			const UnicodeString MAXSTATUSLENGTH = "AQQ/System/MaxStatusLength";
			const UnicodeString MODULESLOADED = "AQQ/System/ModulesLoaded";
			const UnicodeString MSGCOMPOSING = "AQQ/System/MsgComposing";
			const UnicodeString MSGCONTEXT_CLOSE = "AQQ/System/OnMsgContent/Close";
			const UnicodeString MSGCONTEXT_POPUP = "AQQ/System/OnMsgContent/Popup";
			const UnicodeString MULTIPOPUP = "AQQ/System/MultiPopup";
			const UnicodeString NEWSSOURCE_ACTIVE = "AQQ/System/NewsSource/Active";
			const UnicodeString NEWSSOURCE_BEFOREFETCH = "AQQ/System/NewsSource/BeforeFetch";
			const UnicodeString NEWSSOURCE_DELETE = "AQQ/System/NewsSource/Delete";
			const UnicodeString NEWSSOURCE_FETCH = "AQQ/System/NewsSource/Fetch";
			const UnicodeString NOTIFICATIONCLOSED = "AQQ/System/NotificationClosed";
			const UnicodeString ONLINECHECK = "AQQ/System/OnlineCheck";
			const UnicodeString PERFORM_COPYDATA = "AQQ/System/Perform/CopyData";
			const UnicodeString PLAYSOUND = "AQQ/System/Plugin/PlaySound";
			const UnicodeString PLUGIN_BEFOREUNLOAD = "AQQ/System/Plugin/BeforeUnload";
			const UnicodeString PLUGIN_REFRESHLIST = "AQQ/System/Plugin/RefreshList";
			const UnicodeString POPUP = "AQQ/System/Popup";
			const UnicodeString PRECONFIG = "AQQ/System/PreConfig";
			const UnicodeString QUERY = "AQQ/System/Query";
			const UnicodeString QUERYEX = "AQQ/System/QueryEx";
			const UnicodeString SETLASTSTATE = "AQQ/System/SetLastState";
			const UnicodeString SETNOTE = "AQQ/System/SetNote";
			const UnicodeString SMSCONFIG = "AQQ/System/SMSConfig";
			const UnicodeString SMSSEND = "AQQ/System/SMSSend";
			const UnicodeString SMSSUPPORTED = "AQQ/System/SMSSupported";
			const UnicodeString STATECHANGE = "AQQ/System/StateChange";
			const UnicodeString TABCHANGE = "AQQ/System/TabChange";
			const UnicodeString THEMECHANGED = "AQQ/System/ThemeChanged";
			const UnicodeString THEMEINFO = "AQQ/System/ThemeInfo";
			const UnicodeString THEMESTART = "AQQ/System/ThemeStart";
			const UnicodeString TOOLTIP_BEFORESHOW = "AQQ/System/ToolTip/BeforeShow";
			const UnicodeString TOOLTIP_FILL = "AQQ/System/ToolTip/Fill";
			const UnicodeString TRANSFER_STATUS_CHANGE = "AQQ/System/Transfer/Status/Change";
			const UnicodeString TRAY_CLICK = "AQQ/System/Tray/Click";
			const UnicodeString TRAYICONIMAGE = "AQQ/System/TrayIconImage";
			const UnicodeString TRAYICONIMAGEPATH = "AQQ/System/TrayIconImagePath";
			const UnicodeString WINDOWEVENT = "AQQ/System/WindowEvent";
			const UnicodeString XMLDEBUG = "AQQ/System/XMLDebug";
			const UnicodeString XMLIDDEBUG = "AQQ/System/XMLIDDebug";
		}
		namespace Windows {
			const UnicodeString PRESETNOTE_NOTE = "AQQ/Window/PreSetNote/Note";
			const UnicodeString SETNOTE_CLOSE = "AQQ/Window/SetNote/Close";
			const UnicodeString SETNOTE_NOTEJID = "AQQ/Window/SetNote/NoteJID";
			const UnicodeString SETNOTE_PUTNOTE = "AQQ/Window/SetNote/PutNote";
			const UnicodeString SETSTATUS = "AQQ/Window/SetStatus";
			const UnicodeString TRANSPARENT_ = "AQQ/Window/Transparent";
		}
		namespace Controls {
			const UnicodeString WEBBROWSER_BEFORENAV = "AQQ/Controls/WebBrowser/BeforeNav";
			const UnicodeString WEBBROWSER_NAVCOMPLETE = "AQQ/Controls/WebBrowser/NavComplete";
			const UnicodeString WEBBROWSER_STATUSCHANGE = "AQQ/Controls/WebBrowser/StatusChange";
			const UnicodeString WEBBROWSER_TITLECHANGE = "AQQ/Controls/WebBrowser/TitleChange";
		}
		namespace Contacts {
			const UnicodeString ADD = "AQQ/Contacts/Add";
			const UnicodeString ADD_BAN = "AQQ/Contacts/Add/Ban";
			const UnicodeString ADDLINE = "AQQ/Contacts/AddLine";
			const UnicodeString ATTENTION = "AQQ/Contacts/Attention";
			const UnicodeString BUDDY_ACTIVETAB = "AQQ/Contacts/Buddy/ActiveTab";
			const UnicodeString BUDDY_CLOSETAB = "AQQ/Contacts/Buddy/CloseTab";
			const UnicodeString BUDDY_CLOSETABMESSAGE = "AQQ/Contacts/Buddy/CloseTabMessage";
			const UnicodeString BUDDY_CONFERENCETAB = "AQQ/Contacts/Buddy/ConferenceTab";
			const UnicodeString BUDDY_FETCHALLTABS = "AQQ/Contacts/Buddy/FetchAllTabs";
			const UnicodeString BUDDY_FETCHSELECTED = "AQQ/Contacts/Buddy/FetchSelected";
			const UnicodeString BUDDY_FORMACTIVATE = "AQQ/Contacts/Buddy/FormActive";
			const UnicodeString BUDDY_PRIMARYTAB = "AQQ/Contacts/Buddy/PrimaryTab";
			const UnicodeString BUDDY_SELECTED = "AQQ/Contacts/Buddy/Selected";
			const UnicodeString BUDDY_TABCAPTION = "AQQ/Contacts/Buddy/TabCaption";
			const UnicodeString BUDDY_TABIMAGE = "AQQ/Contacts/Buddy/TabImage";
			const UnicodeString BUDDYFILL = "AQQ/Contacts/BuddyFill";
			const UnicodeString CHANGEGROUPNAME = "AQQ/Contacts/ChangeGroupName";
			const UnicodeString CHANGENAME = "AQQ/Contacts/ChangeName";
			const UnicodeString DELETE_ = "AQQ/Contacts/Delete";
			const UnicodeString FROMPLUGIN = "AQQ/Contacts/FromPlugin";
			const UnicodeString GETSEARCHXML = "AQQ/Contacts/GetSearchXML";
			const UnicodeString GETVCARD = "AQQ/Contacts/GetVCard";
			const UnicodeString ICONSHOWTYPE_HDINDEX = "AQQ/Contacts/IconShowType/HDIndex";
			const UnicodeString ICONSHOWTYPE_INDEX = "AQQ/Contacts/IconShowType/Index";
			const UnicodeString ICONSHOWTYPE_PATH = "AQQ/Contacts/IconShowType/Path";
			const UnicodeString ICONSHOWTYPEJID_INDEX = "AQQ/Contacts/IconShowTypeJID/Index";
			const UnicodeString LASTSEARCHID = "AQQ/Contacts/LastSearchID";
			const UnicodeString LISTREADY = "AQQ/Contacts/ListReady";
			const UnicodeString OFFLINE = "AQQ/Contacts/Offline";
			const UnicodeString PRESENDMSG = "AQQ/Contacts/PreSendMsg";
			const UnicodeString RECVMSG = "AQQ/Contacts/RecvMsg";
			const UnicodeString REMOVE_BAN = "AQQ/Contacts/Remove/Ban";
			const UnicodeString REPLYLIST = "AQQ/Contacts/ReplyList";
			const UnicodeString REPLYLISTEND = "AQQ/Contacts/ReplyListEnd";
			const UnicodeString REQUESTBUDDY = "AQQ/Contacts/RequestBuddy";
			const UnicodeString RESETAVATAR = "AQQ/Contacts/ResetAvatar";
			const UnicodeString SENDFILE = "AQQ/Contacts/SendFile";
			const UnicodeString SENDMSG = "AQQ/Contacts/SendMsg";
			const UnicodeString SENDPIC = "AQQ/Contacts/SendPic";
			const UnicodeString SENDPIC_SIZECHECK = "AQQ/Contacts/SendPic/SizeCheck";
			const UnicodeString SETHTMLSTATUS = "AQQ/Contacts/SetHTMLStatus";
			const UnicodeString SETINVISIBLE = "AQQ/Contacts/SetInvisible";
			const UnicodeString SETSEARCHXML = "AQQ/Contacts/SetSearchXML";
			const UnicodeString STATUSCAPTION = "AQQ/Contacts/StatusCaption";
			const UnicodeString UPDATE = "AQQ/Contacts/Update";
			const UnicodeString UPDATEGROUPS = "AQQ/Contacts/UpdateGroups";
			const UnicodeString VALIDATEJID = "AQQ/Contacts/ValidateJID";
		}
		namespace Misc {
			const UnicodeString SHOWINFO = "AQQ/Function/ShowInfo";
		}
	}

	namespace Functions {
		namespace System {
			const UnicodeString ACCOUNT_EVENTS = "AQQ/System/Account/Events";
			const UnicodeString ADDIMPEXP = "AQQ/System/AddImpExp";
			const UnicodeString ADDSMSGATE = "AQQ/System/AddSMSGate";
			const UnicodeString APPVER = "AQQ/System/AppVer";
			const UnicodeString CHANGE_JABBERRESOURCES = "AQQ/System/Change/Resources";
			const UnicodeString CHANGESMS_ID = "AQQ/System/ChangeSMS/ID";
			const UnicodeString CHANGESMS_STATUS = "AQQ/System/ChangeSMS/Status";
			const UnicodeString CHAT = "AQQ/System/Chat";
			const UnicodeString CHAT_OPEN = "AQQ/System/Chat/Open";
			const UnicodeString CHAT_PRESENCE = "AQQ/System/Chat/Presence";
			const UnicodeString COLORCHANGE = "AQQ/System/ColorChange";
			const UnicodeString COLORCHANGEV2 = "AQQ/System/ColorChangeV2";
			const UnicodeString COLORGETBRIGHTNESS = "AQQ/System/ColorGetBrightness";
			const UnicodeString COLORGETHUE = "AQQ/System/ColorGetHue";
			const UnicodeString COLORGETSATURATION = "AQQ/System/ColorGetSaturation";
			const UnicodeString DEBUG_XML = "AQQ/System/Debug/XML";
			const UnicodeString ERROR_ = "AQQ/System/Error";
			const UnicodeString FRAMESETTINGS = "AQQ/System/FrameSettings";
			const UnicodeString SETENABLED = "AQQ/System/SetEnabled";
			const UnicodeString GETNEWCACHEITEMPATH = "AQQ/System/GetNewCacheItemPath";
			const UnicodeString INTERPRET_XML = "AQQ/System/Interpret/XML";
			const UnicodeString MODULESLOADED = "AQQ/System/ModulesLoaded";
			const UnicodeString MSGCOMPOSING = "AQQ/System/MsgComposing";
			const UnicodeString NEWSSOURCE_ADD = "AQQ/System/NewsSource/Add";
			const UnicodeString NEWSSOURCE_DELETE = "AQQ/System/NewsSource/Delete";
			const UnicodeString NEWSSOURCE_FETCHEND = "AQQ/System/NewsSource/FetchEnd";
			const UnicodeString NEWSSOURCE_FETCHSTART = "AQQ/System/NewsSource/FetchStart";
			const UnicodeString NEWSSOURCE_ITEM = "AQQ/System/NewsSource/Item";
			const UnicodeString NEWSSOURCE_REFRESH = "AQQ/System/NewsSource/Refresh";
			const UnicodeString NEWSSOURCE_UPDATE = "AQQ/System/NewsSource/Update";
			const UnicodeString ONCONNECT_SILENCE = "AQQ/System/OnConnect/Silence";
			const UnicodeString PLAYSOUND = "AQQ/System/Plugin/PlaySound";
			const UnicodeString PLUGIN_ACTIVE = "AQQ/System/Plugin/Active";
			const UnicodeString PLUGIN_EXCLUDE = "AQQ/System/Plugin/Exclude";
			const UnicodeString PLUGIN_REFRESHLIST = "AQQ/System/Plugin/RefreshList";
			const UnicodeString REGEXP = "AQQ/System/RegExp";
			const UnicodeString REMOVEAGENT = "AQQ/System/RemoveAgent";
			const UnicodeString REMOVESMSGATE = "AQQ/System/RemoveSMSGate";
			const UnicodeString RENAMESMSGATE = "AQQ/System/RenameSMSGate";
			const UnicodeString RESTART = "AQQ/System/Restart";
			const UnicodeString RUNACTION = "AQQ/System/RunAction";
			const UnicodeString SENDHOOK = "AQQ/System/SendHook";
			const UnicodeString SENDXML = "AQQ/System/SendXML";
			const UnicodeString SETAGENT = "AQQ/System/SetAgent";
			const UnicodeString SETSHOWANDSTATUS = "AQQ/System/SetShowAndStatus";
			const UnicodeString SETUPDATELINK = "AQQ/System/SetUpdateLink";
			const UnicodeString SKYPE_CONVSERSATION = "AQQ/System/Skype/Conversation";
			const UnicodeString SOCIALSEND = "AQQ/System/SocialSend";
			const UnicodeString THEME_APPLY = "AQQ/System/Theme/Apply";
			const UnicodeString THEME_REFRESH = "AQQ/System/Theme/Refresh";
			const UnicodeString THEME_SET = "AQQ/System/Theme/Set";
			const UnicodeString TOOLTIP_ADDITEM = "AQQ/System/ToolTip/AddItem";
			const UnicodeString TOOLTIP_SHOW = "AQQ/System/ToolTip/Show";
			const UnicodeString TRANSFER_STATUS_CHANGE = "AQQ/System/Transfer/Status/Change";
			const UnicodeString TRAYICONREFRESH = "AQQ/System/TrayIconRefresh";
		}
		namespace Controls {
			const UnicodeString CREATEBUTTON = "/CreateButton";
			const UnicodeString CREATEPOPUPMENU = "AQQ/Controls/CreatePopUpMenu";
			const UnicodeString CREATEPOPUPMENUITEM = "AQQ/Controls/CreatePopUpMenuItem";
			const UnicodeString DESTROYBUTTON = "/DestroyButton";
			const UnicodeString DESTROYPOPUPMENU = "AQQ/Controls/DestroyPopUpMenu";
			const UnicodeString DESTROYPOPUPMENUITEM = "AQQ/Controls/DestroyPopUpMenuItem";
			const UnicodeString EDITPOPUPMENUITEM = "AQQ/Controls/EditPopUpMenuItem";
			const UnicodeString FORM_CONTROL = "AQQ/Controls/Form/Control";
			const UnicodeString FORM_CREATE = "AQQ/Controls/Form/Create";
			const UnicodeString FORM_DESTROY = "AQQ/Controls/Form/Destroy";
			const UnicodeString FORM_GETMODALRESULT = "AQQ/Controls/Form/GetModalResult";
			const UnicodeString FORM_VISIBLE = "AQQ/Controls/Form/Destroy";
			const UnicodeString GETPOPUPMENUITEM = "AQQ/Controls/GetPopupMenuItem";
			const UnicodeString MAINSTATUS_SETPANELTEXT = "AQQ/Controls/MainStatus/SetPanelText";
			const UnicodeString SMSENABLE = "AQQ/Controls/SMSControls/Enable";
			const UnicodeString TOOLBAR = "AQQ/Controls/Toolbar/";
			const UnicodeString UPDATEBUTTON = "/UpdateButton";
			const UnicodeString WEBBROWSER_CLICKID = "AQQ/Controls/WebBrowser/ClickID";
			const UnicodeString WEBBROWSER_CREATE = "AQQ/Controls/WebBrowser/Create";
			const UnicodeString WEBBROWSER_DESTROY = "AQQ/Controls/WebBrowser/Destroy";
			const UnicodeString WEBBROWSER_GETID = "AQQ/Controls/WebBrowser/GetID";
			const UnicodeString WEBBROWSER_NAVIGATE = "AQQ/Controls/WebBrowser/Navigate";
			const UnicodeString WEBBROWSER_SETID = "AQQ/Controls/WebBrowser/SetID";
		}
		namespace Contacts {
			const UnicodeString ACKMSG = "AQQ/Contacts/AckMsg";
			const UnicodeString ADD_BAN = "AQQ/Contacts/Add/Ban";
			const UnicodeString ADDFORM = "AQQ/Contacts/AddForm";
			const UnicodeString ADDLINEINFO = "AQQ/Contacts/AddLineInfo";
			const UnicodeString BUDDY_CLOSETAB = "AQQ/Contacts/Buddy/CloseTab";
			const UnicodeString BUDDY_FETCHALLTABS = "AQQ/Contacts/Buddy/FetchAllTabs";
			const UnicodeString BUDDY_FETCHSELECTED = "AQQ/Contacts/Buddy/FetchSelected";
			const UnicodeString BUDDY_TABCAPTION = "AQQ/Contacts/Buddy/TabCaption";
			const UnicodeString BUDDY_TABIMAGE = "AQQ/Contacts/Buddy/TabImage";
			const UnicodeString CREATE = "AQQ/Contacts/Create";
			const UnicodeString DELETE_ = "AQQ/Contacts/Delete";
			const UnicodeString DESTROYDOMAIN = "AQQ/Contacts/DestroyDomain";
			const UnicodeString ERRSEARCHXML = "AQQ/Contacts/ErrSearchXML";
			const UnicodeString FILLSIMPLEINFO = "AQQ/Contacts/FillSimpleInfo";
			const UnicodeString GET_SIMPLEINFO = "AQQ/Contacts/Get/SimpleInfo";
			const UnicodeString GETHTMLSTATUS = "AQQ/Contacts/GetHTMLStatus";
			const UnicodeString GETINVISIBLE = "AQQ/Contacts/GetInvisible";
			const UnicodeString HAVE_BAN = "AQQ/Contacts/Have/Ban";
			const UnicodeString _MESSAGE = "AQQ/Contacts/Message";
			const UnicodeString REMOVE_BAN = "AQQ/Contacts/Remove/Ban";
			const UnicodeString REQUESTBUDDY = "AQQ/Contacts/RequestBuddy";
			const UnicodeString REQUESTLIST = "AQQ/Contacts/RequestList";
			const UnicodeString RESSEARCHXML = "AQQ/Contacts/ResSearchXML";
			const UnicodeString RESVCARD = "AQQ/Contacts/ResVCard";
			const UnicodeString SENDMSG = "AQQ/Contacts/SendMsg";
			const UnicodeString SENDPIC = "AQQ/Contacts/SendPic";
			const UnicodeString SET_AVATAR = "AQQ/Contacts/Set/Avatar";
			const UnicodeString SET_SIMPLEINFO = "AQQ/Contacts/Set/SimpleInfo";
			const UnicodeString SETHTMLSTATUS = "AQQ/Contacts/SetHTMLStatus";
			const UnicodeString SETINVISIBLE = "AQQ/Contacts/SetInvisible";
			const UnicodeString SETOFFLINE = "AQQ/Contacts/SetOffline";
			const UnicodeString SETWEB_AVATAR = "AQQ/Contacts/SetWeb/Avatar";
			const UnicodeString UPDATE = "AQQ/Contacts/Update";
		}
		namespace Icons {
			const UnicodeString LOADPNGICON = "AQQ/Icons/LoadPNGIcon";
			const UnicodeString REPLACEPNGICON = "AQQ/Icons/ReplacePNGIcon";
			const UnicodeString DESTROYPNGICON = "AQQ/Icons/DestroyPNGIcon";
		}
		namespace Misc {
			const UnicodeString BASE64 = "AQQ/Function/Base64";
			const UnicodeString CONVERTTOXML = "AQQ/Function/ConvertToXML";
			const UnicodeString DEFAULTNICK = "AQQ/Function/DefaultNick";
			const UnicodeString EXCLUDETRAILINGPATHDELIMITER = "AQQ/Function/ExcludeTrailingPathDelimiter";
			const UnicodeString EXECUTEMSG = "AQQ/Function/ExecuteMsg";
			const UnicodeString EXECUTEMSG_NOPRIORITY = "AQQ/Function/ExecuteMsg/NoPriority";
			const UnicodeString EXTRACTFILENAME = "AQQ/Function/ExtractFilename";
			const UnicodeString FETCHSETUP = "AQQ/Function/FetchSetup";
			const UnicodeString FILEEXISTS = "AQQ/Function/FileExists";
			const UnicodeString FINDFILES = "AQQ/Function/FindFiles";
			const UnicodeString FORCEDIRECTORIES = "AQQ/Function/ForceDirectories";
			const UnicodeString GETAPPFILEPATH = "AQQ/Function/GetAppFilePath";
			const UnicodeString GETAPPPATH = "AQQ/Function/GetAppPath";
			const UnicodeString GETDEFLANGCODE = "AQQ/Function/GetDefLangCode";
			const UnicodeString GETEXTERNALIP = "AQQ/Function/GetExternalIP";
			const UnicodeString GETLANGCODE = "AQQ/Function/GetLangCode";
			const UnicodeString GETLANGSTR = "AQQ/Function/GetLangStr";
			const UnicodeString GETNETWORKINFO = "AQQ/Function/GetNetworkInfo";
			const UnicodeString GETNETWORKSTATE = "AQQ/Function/GetNetworkState";
			const UnicodeString GETNUMID = "AQQ/Function/GetNumID";
			const UnicodeString GETPLUGINDIR = "AQQ/Function/GetPluginDir";
			const UnicodeString GETPLUGINUSERDIR = "AQQ/Function/GetPluginUserDir";
			const UnicodeString GETPNGHD_FILEPATH = "AQQ/Function/GetPNGHD/FilePath";
			const UnicodeString GETPNG_FILEPATH = "AQQ/Function/GetPNG/FilePath";
			const UnicodeString GETPROXY = "AQQ/Function/GetProxy";
			const UnicodeString GETSTARTCOUNT = "AQQ/Function/GetStartCount";
			const UnicodeString GETSTATEPNG_FILEPATH = "AQQ/Function/GetStatePNG/FilePath";
			const UnicodeString GETSTATEPNG_INDEX = "AQQ/Function/GetStatePNG/Index";
			const UnicodeString GETSTRID = "AQQ/Function/GetStrID";
			const UnicodeString GETTHEMEDIR = "AQQ/Function/GetThemeDir";
			const UnicodeString GETTHEMEDIRRW = "AQQ/Function/GetThemeDirRW";
			const UnicodeString GETTOKEN = "AQQ/Function/GetToken";
			const UnicodeString GETUSERDIR = "AQQ/Function/GetUserDir";
			const UnicodeString GETUSEREXCOUNT = "AQQ/Function/GetUserExCount";
			const UnicodeString GETUSERUID = "AQQ/Function/GetUserUID";
			const UnicodeString INI_CREATE = "AQQ/Function/INI/Create";
			const UnicodeString INI_FREE = "AQQ/Function/INI/Free";
			const UnicodeString INI_READSTRING = "AQQ/Function/INI/ReadString";
			const UnicodeString INI_WRITESTRING = "AQQ/Function/INI/WriteString";
			const UnicodeString INTTOSTR = "AQQ/Function/IntToStr";
			const UnicodeString ISLISTREADY = "AQQ/Function/IsListReady";
			const UnicodeString ISVERSIONHIGHER = "AQQ/Function/IsVersionHigher";
			const UnicodeString LASTDELIMITER = "AQQ/Function/LastDelimiter";
			const UnicodeString LOADLASTCONV = "AQQ/Function/LoadLastConv";
			const UnicodeString LOG = "AQQ/Function/Log";
			const UnicodeString MSGWINDOW = "AQQ/Function/Msgwindow";
			const UnicodeString NORMALIZE = "AQQ/Function/Normalize";
			const UnicodeString OPENURL = "AQQ/Function/OpenURL";
			const UnicodeString OPENYTURL = "AQQ/Function/OpenYTURL";
			const UnicodeString PROCESSMESSAGES = "AQQ/Function/ProcessMessages";
			const UnicodeString QUOTEDSTR = "AQQ/Function/QuotedStr";
			const UnicodeString REFRESHSETUP = "AQQ/Function/RefreshSetup";
			const UnicodeString SAVERESOURCE = "AQQ/Function/SaveResource";
			const UnicodeString SAVESETUP = "AQQ/Function/SaveSetup";
			const UnicodeString SAY = "AQQ/Function/Say";
			const UnicodeString SHA1 = "AQQ/Function/Sha1";
			const UnicodeString SHOWINFO = "AQQ/Function/ShowInfo";
			const UnicodeString SHOWMESSAGE = "AQQ/Function/ShowMessage";
			const UnicodeString SILENTUPDATECHECK = "AQQ/Function/SilentUpdateCheck";
			const UnicodeString SSLHANDLE = "AQQ/Function/SSLHandle";
			const UnicodeString SSLLIBHANDLE = "AQQ/Function/SSLLibHandle";
			const UnicodeString STATETOSTR = "AQQ/Function/StateToStr";
			const UnicodeString STRTOINTDEF = "AQQ/Function/StrToIntDef";
			const UnicodeString TABCOUNT = "AQQ/Function/TabCount";
			const UnicodeString TABINDEX = "AQQ/Function/TabIndex";
			const UnicodeString TABMOVE = "AQQ/Function/TabMove";
			const UnicodeString TABWASCLOSED = "AQQ/Function/TabWasClosed";
			const UnicodeString URLEX = "AQQ/Function/URLEx";
			const UnicodeString URLGET = "AQQ/Function/URLGet";
			const UnicodeString URLPOST = "AQQ/Function/URLPost";
		}
	}
}

namespace AQQAPI {
	namespace Constants {
		namespace AccountEvents {
			const int DEFAULT = 0;
			const int NEW = 1;
			const int EDIT = 2;
			const int DELETE_ = 3;
			const int CHANGEPASS = 4;
		}
		namespace SystemFunctions {
			const int SEARCHONLIST = 1;
			const int ANTISPIM_LEN = 2;
			const int TASKBARPEN = 3;
			const int CLOSEBTN = 4;
			const int MSGCOUNTER = 5;
			const int SKINSYSDLG = 6;
		}
		namespace Tabs {
			const int JABBER = 1;
			const int SMS = 2;
			const int MULTICHAT = 3;
			const int NEWS = 4;
		}
		namespace SetNote {
			const int ABORT = 1;
			const int STATE = 2;
			const int JID = 3;
			const int CHECK = 4;
		}
		namespace ChatMode {
			const int NORMAL = 1;
			const int SIMPLE = 2;
		}
		namespace Role {
			const UnicodeString VISITOR = "visitor";
			const UnicodeString MODERATOR = "moderator";
			const UnicodeString PARTICIPANT = "participant";
			const UnicodeString OBSERVER = "none";
		}
		namespace Affiliation {
			const UnicodeString ADMIN = "admin";
			const UnicodeString OUTCAST = "outcast";
			const UnicodeString MEMBER = "member";
			const UnicodeString OWNER = "owner";
			const UnicodeString NONE = "none";
		}
		namespace ConnectionState {
			const int DISCONNECTED = 1;
			const int CONNECTING = 2;
			const int CONNECTED = 3;
		}
		namespace TooltipEvent {
			const int TITLE = 0;
			const int DATA = 1;
			const int STATUS = 2;
			const int AUTH = 3;
			const int ACTIVITY = 4;
		}
		namespace ShowMessageType {
			const int INFO = 0;
			const int WARN = 1;
			const int QUESTION = 2;
		}
		namespace WindowStatus {
			const int NONE = 0;
			const int SET = 1;
			const int SETEXIT = 2;
		}
		namespace Cache {
			const UnicodeString ITEM = "AQQ_CACHE_ITEM";
			const UnicodeString GHOSTITEM = "AQQ_CACHE_GHOSTITEM";
		}
		namespace Query {
			const int DELETE_ = 1;
			const int _MESSAGE = 2;
			const int SENDFILE = 3;
			const int AUTHREQUEST = 4;
			const int SENDPIC = 5;
			const int AUTH = 6;
			const int CANCELAUTH = 7;
			const int SMS = 8;
			const int VCARD = 9;
			const int ARCHIVE = 10;
			const int NETWORK = 11;
			const int HAVEVCARD = 12;
			const int ATTENTION = 13;
			const int NETWORKSTATE = 14;
			const int STATE = 15;
			const int NOTE = 16;
			const int ONLINE = 17;
			const int FFC = 18;
			const int AWAY = 19;
			const int XA = 20;
			const int DND = 21;
			const int OFFLINE = 22;
		}
		namespace ContactUpdate {
			const int OFFLINE = -1;
			const int NORMAL = 0;
			const int NOOFFLINE = 1;
			const int ONLINE = 2;
			const int ONLYSTATUS = 3;
		}
		namespace State {
			const int OFFLINE = 0;
			const int ONLINE = 1;
			const int FFC = 2;
			const int AWAY = 3;
			const int XA = 4;
			const int DND = 5;
			const int INVISIBLE = 6;
			const int NULLS = 7;
		}
		namespace Debug {
			const int XMLIN = 0;
			const int XMLOUT = 1;
		}
		namespace OnCheck {
			const int COPYTO = 1;
			const int DELETE_ = 2;
			const int MOVETO = 3;
			const int CHANGENAME = 4;
			const int SENDMESSAGE = 5;
			const int SENDIMAGE = 6;
			const int SETGROUPS = 7;
			const int VCARD = 8;
		}
		namespace ChatState {
			const int NONE = 0;
			const int ACTIVE = 1;
			const int COMPOSING = 2;
			const int GONE = 3;
			const int INACTIVE = 4;
			const int PAUSED = 5;
		}
		namespace SMSStatus {
			const int FAILED = 0;
			const int OK = 1;
			const int DELIVERED = 2;
			const int NOTDELIVERED = 3;
		}
		namespace SMSSend {
			const int OK = 1;
			const int FAILED = 2;
			const int FAILED_SET = 3;
			const int FAILED_AUTH = 4;
			const int FAILED_CONNECT = 5;
			const int FAILED_LIMIT = 6;
			const int FAILED_TOKEN = 7;
			const int FAILED_NOSUPPORT = 8;
			const int FAILED_UNKNOWN = 9;
			const int OK_SILENT = 10;
			const int OK_SILENT_NOENABLE = 11;
			const int FAILED_FILTER = 12;
			const int FAILED_TOOLONG = 13;
			const int FAILED_ABORTED = 14;
		}
		namespace Sound {
			const int FIRSTIN = 0;
			const int IN_ = 1;
			const int OUT_ = 2;
			const int STATUS = 3;
			const int TRANSFER = 4;
			const int ENDTRANSFER = 5;
			const int LOCK = 6;
			const int HELP = 7;
			const int CLOSEHELP = 8;
			const int CHATIN = 9;
			const int CHATOUT = 10;
			const int ATTENTION = 11;
			const int CALLIN = 12;
			const int CALLEND = 13;
			const int INFOBOX = 14;
			const int POINTS = 15;
			const int AUTH = 16;
			const int NEWS = 17;
			const int CLICK = 18;
		}
		namespace ImpExpType {
			const int EXPORT = 0;
			const int IMPORT = 1;
			const int OTHER = 2;
		}
		namespace Align {
			const int TOP = 0;
			const int BOTTOM = 1;
			const int LEFT = 2;
			const int RIGHT = 3;
			const int CLIENT = 4;
			const int TOWINDOW = 5;
		}
		namespace MessageKind {
			const int CHAT = 0;
			const int GROUPCHAT = 1;
			const int RTT = 2;
		}
		namespace Transfer {
			const int DECLINE = 0;
			const int ACCEPT = 1;
			const int CONNECTING = 2;
			const int ACTIVE = 3;
			const int DISCONNECT = 4;
			const int FINISHED = 5;
			const int PAUSED = 6;
			const int ABORTED = 7;
		}
		namespace WindowEvent {
			const int CREATE = 1;
			const int CLOSE = 2;
		}
		namespace Authorization {
			const int BOTH = 0;
			const int NONE = 1;
			const int FROM = 2;
			const int TO = 3;
			const int REMOVE = 4;
			const int NONEASK = 5;
			const int FROMASK = 6;
		}
		namespace Socks {
			const int _5 = 0;
			const int _4A = 1;
			const int _4 = 2;
		}
		namespace MouseButton {
			const int LEFT = 0;
			const int RIGHT = 1;
			const int DBLCLICK = 2;
		}
		namespace ControlAction {
			const int CREATE = 0;
			const int DESTROY = 1;
			const int SET = 2;
			const int GET = 3;
			const int SETFOCUS = 4;
		}
		namespace ModalResult {
			const int NONE = 0;
			const int OK = 1;
			const int CANCEL = 2;
		}
	}

	namespace Enumerations {
		enum THintEvent {
			theAuth,
			theTitle,
			theData,
			theStatus,
			theActivity
		};

		enum TMiniEvent {
			tmeDeleted,
			tmeStatus,
			tmePseudoStatus,
			tmePseudoMsg,
			tmeMsg,
			tmePseudoMsgCap,
			tmeMsgCap,
			tmeInfo,
			tmeAction,
			tmeAbuse,
			tmeVoip
		};

		enum TAddonType {
			tatUnCheck,
			tatUnknown,
			tatVisualStyle,
			tatVisualStylePatch,
			tatSmileys,
			tatPlugin
		};
	}

	namespace Types {
		typedef struct TPluginChatPrep
		{
			int cbSize;
			int UserIdx;
			UnicodeString JID;
			UnicodeString Channel;
			bool CreateNew;
			bool Fast;
		} TPluginChatPrep;
		typedef
			TPluginChatPrep* PPluginChatPrep;

		typedef struct TPluginChatPresence
		{
			int cbSize;
			int UserIdx;
			UnicodeString JID;
			UnicodeString RealJID;
			UnicodeString Affiliation;
			UnicodeString Role;
			bool Offline;
			UnicodeString Nick;
			bool Kicked;
			bool Banned;
			UnicodeString Ver;
			UnicodeString JIDToShow;
		} TPluginChatPresence;
		typedef
			TPluginChatPresence* PPluginChatPresence;

		typedef struct TPluginChatOpen
		{
			int cbSize;
			UnicodeString JID;
			int UserIdx;
			UnicodeString Channel;
			UnicodeString Nick;
			bool NewWindow;
			bool IsNewMsg;
			bool Priority;
			UnicodeString OriginJID;
			int ImageIndex;
			bool AutoAccept;
			unsigned char ChatMode;
		} TPluginChatOpen;
		typedef
			TPluginChatOpen* PPluginChatOpen;

		typedef struct TPluginHook
		{
			UnicodeString HookName;
			WPARAM wParam;
			LPARAM lParam;
		} TPluginHook;
		typedef
			TPluginHook* PPluginHook;

		typedef struct TSaveSetup
		{
			UnicodeString Section;
			UnicodeString Ident;
			UnicodeString Value;
		} TSaveSetup;
		typedef
			TSaveSetup* PSaveSetup;

		typedef struct TPluginSong
		{
			UnicodeString Title;
			int Position;
			int Length;
		} TPluginSong;
		typedef
			TPluginSong* PPluginSong;

		typedef struct TPluginAddForm
		{
			int UserIdx;
			UnicodeString JID;
			UnicodeString Nick;
			UnicodeString Agent;
			bool Modal;
			bool Custom;
		} TPluginAddForm;
		typedef
			TPluginAddForm* PPluginAddForm;

		typedef struct TPluginShowInfo
		{
			int cbSize;
			AQQAPI::Enumerations::TMiniEvent Event;
			UnicodeString Text;
			UnicodeString ImagePath;
			int TimeOut;
			UnicodeString ActionID;
			unsigned  int Tick;
		} TPluginShowInfo;
		typedef
			TPluginShowInfo* PPluginShowInfo;

		typedef struct TPluginToolTipItem
		{
			int cbSize;
			unsigned  int Tick;
			int Event;
			UnicodeString Text;
			UnicodeString ImagePath;
			UnicodeString ActionID;
			int Y1;
			int Y2;
			int TimeOut;
			int Flag;
		} TPluginToolTipItem;
		typedef
			TPluginToolTipItem* PPluginToolTipItem;

		typedef struct TPluginToolTipID
		{
			int cbSize;
			UnicodeString ID;
			UnicodeString Name;
		} TPluginToolTipID;
		typedef
			TPluginToolTipID* PPluginToolTipID;

		typedef struct TPluginColorChange
		{
			int cbSize;
			int Hue;
			int Saturation;
			int Brightness;
		} TPluginColorChange;
		typedef
			TPluginColorChange* PPluginColorChange;

		typedef struct TPluginSMSResult
		{
			int cbSize;
			UnicodeString ID;
			int Result;
		} TPluginSMSResult;
		typedef
			TPluginSMSResult* PPluginSMSResult;

		typedef struct TPluginXMLChunk
		{
			int cbSize;
			UnicodeString ID;
			UnicodeString From;
			UnicodeString XML;
			int UserIdx;
		} TPluginXMLChunk;
		typedef
			TPluginXMLChunk* PPluginXMLChunk;

		typedef struct TPluginContactSimpleInfo
		{
			int cbSize;
			UnicodeString JID;
			UnicodeString First;
			UnicodeString Last;
			UnicodeString Nick;
			UnicodeString CellPhone;
			UnicodeString Phone;
			UnicodeString Mail;
		} TPluginContactSimpleInfo;
		typedef
			TPluginContactSimpleInfo* PPluginContactSimpleInfo;

		typedef struct TPluginItemDescriber
		{
			int cbSize;
			unsigned  int FormHandle;
			UnicodeString ParentName;
			UnicodeString Name;
		} TPluginItemDescriber;
		typedef
			TPluginItemDescriber* PPluginItemDescriber;

		typedef struct TPluginPopUp
		{
			int cbSize;
			UnicodeString Name;
			unsigned  int ParentHandle;
			unsigned  int Handle;
		} TPluginPopUp;
		typedef
			TPluginPopUp* PPluginPopUp;

		typedef struct TPluginTriple
		{
			int cbSize;
			unsigned  int Handle1;
			unsigned  int Handle2;
			unsigned  int Handle3;
			int Param1;
			int Param2;
		} TPluginTriple;
		typedef
			TPluginTriple* PPluginTriple;

		typedef struct TPluginError
		{
			int cbSize;
			UnicodeString ID;
			UnicodeString Desc;
			UnicodeString LangID;
			UnicodeString ErrorCode;
		} TPluginError;
		typedef
			TPluginError* PPluginError;

		typedef struct TPluginTransfer
		{
			int cbSize;
			UnicodeString ID;
			UnicodeString FileName;
			UnicodeString Location;
			int Status;
			UnicodeString TextInfo;
			__int64 FileSize;
			__int64 Position;
			__int64 LastDataSize;
		} TPluginTransfer;
		typedef
			TPluginTransfer* PPluginTransfer;

		typedef struct TPluginTransferOld
		{
			int cbSize;
			UnicodeString ID;
			UnicodeString FileName;
			UnicodeString Location;
			int Status;
			UnicodeString TextInfo;
			unsigned  int FileSize;
			unsigned  int Position;
			unsigned  int LastDataSize;
		} TPluginTransferOld;
		typedef
			TPluginTransferOld* PPluginTransferOld;

		typedef struct TPluginWindowEvent
		{
			int cbSize;
			UnicodeString ClassName;
			unsigned  int Handle;
			int WindowEvent;
		} TPluginWindowEvent;
		typedef
			TPluginWindowEvent* PPluginWindowEvent;

		typedef struct TPluginChatState
		{
			int cbSize;
			UnicodeString Text;
			int Length;
			int SelStart;
			int SelLength;
			unsigned  int ParentHandle;
			unsigned  int Handle;
			int ChatState;
		} TPluginChatState;
		typedef
			TPluginChatState* PPluginChatState;

		typedef struct TPluginWebItem
		{
			int cbSize;
			UnicodeString ID;
			UnicodeString Text;
		} TPluginWebItem;
		typedef
			TPluginWebItem* PPluginWebItem;

		typedef struct TPluginSmallInfo
		{
			int cbSize;
			UnicodeString Text;
		} TPluginSmallInfo;
		typedef
			TPluginSmallInfo* PPluginSmallInfo;

		typedef struct TPluginWebBeforeNavEvent
		{
			int cbSize;
			unsigned  int Handle;
			UnicodeString URL;
			UnicodeString Flags;
			UnicodeString TargetFrameName;
			UnicodeString PostData;
			UnicodeString Headers;
		} TPluginWebBeforeNavEvent;
		typedef
			TPluginWebBeforeNavEvent* PPluginWebBeforeNavEvent;

		typedef struct TPluginWebBrowser
		{
			int cbSize;
			unsigned  int Handle;
			int Top;
			int Left;
			int Width;
			int Height;
			int Align;
			bool RegisterAsDropTarget;
			bool SetVisible;
			bool SetEnabled;
		} TPluginWebBrowser;
		typedef
			TPluginWebBrowser* PPluginWebBrowser;

		typedef struct TPluginDebugInfo
		{
			int cbSize;
			UnicodeString JID;
			UnicodeString XML;
			unsigned char Mode;
		} TPluginDebugInfo;
		typedef
			TPluginDebugInfo* PPluginDebugInfo;

		typedef struct TPluginMaxStatus
		{
			int cbSize;
			int IconIndex;
			UnicodeString Name;
			int Max;
		} TPluginMaxStatus;
		typedef
			TPluginMaxStatus* PPluginMaxStatus;

		typedef struct TPluginProxy
		{
			int cbSize;
			bool ProxyEnabled;
			UnicodeString ProxyServer;
			int ProxyPort;
			int ProxyType;
			bool ProxyAuth;
			UnicodeString ProxyLogin;
			UnicodeString ProxyPass;
		} TPluginProxy;
		typedef
			TPluginProxy* PPluginProxy;

		typedef struct TPluginImpExp
		{
			int cbSize;
			int ImportType;
			UnicodeString Name;
			UnicodeString Service;
			int IconIndex;
		} TPluginImpExp;
		typedef
			TPluginImpExp* PPluginImpExp;

		typedef struct TPluginAutomation
		{
			int cbsize;
			int Flags;
			int NewState;
			double LastActive;
		} TPluginAutomation;
		typedef
			TPluginAutomation* PPluginAutomation;

		typedef struct TPluginStateChange
		{
			int cbSize;
			int OldState;
			int NewState;
			UnicodeString Status;
			bool ByHand;
			int UserIdx;
			UnicodeString JID;
			bool Force;
			UnicodeString Server;
			bool Authorized;
			bool FromPlugin;
			UnicodeString Resource;
		} TPluginStateChange;
		typedef
			TPluginStateChange* PPluginStateChange;

		typedef struct TPluginSMS
		{
			int cbSize;
			UnicodeString CellPhone;
			UnicodeString Msg;
			UnicodeString Sign;
			int GateID;
		} TPluginSMS;
		typedef
			TPluginSMS* PPluginSMS;

		typedef struct TPluginTwoFlagParams
		{
			int cbSize;
			UnicodeString Param1;
			UnicodeString Param2;
			int Flag1;
			int Flag2;
		} TPluginTwoFlagParams;
		typedef
			TPluginTwoFlagParams* PPluginTwoFlagParams;

		typedef struct TPluginAddUser
		{
			int cbSize;
			UnicodeString UID;
			UnicodeString Server;
			UnicodeString Nick;
			UnicodeString Group;
			UnicodeString Reason;
			UnicodeString Service;
		} TPluginAddUser;
		typedef
			TPluginAddUser* PPluginAddUser;

		typedef struct TPluginSMSGate
		{
			int cbSize;
			UnicodeString Name;
			UnicodeString Prompt;
			bool IsConfig;
			int MaxLength;
			int SignMaxLength;
		} TPluginSMSGate;
		typedef
			TPluginSMSGate* PPluginSMSGate;

		typedef struct TPluginAgent
		{
			int cbSize;
			UnicodeString JID;
			UnicodeString Name;
			UnicodeString Prompt;
			bool Transport;
			bool Search;
			bool GroupChat;
			bool Agents;
			UnicodeString Service;
			bool CanRegister;
			UnicodeString Description;
			bool RequiredID;
			int IconIndex;
			UnicodeString PluginAccountName;
		} TPluginAgent;
		typedef
			TPluginAgent* PPluginAgent;

		typedef struct TPluginAvatar
		{
			UnicodeString FileName;
			bool XEPEmpty;
			bool SilentMode;
			UnicodeString JID;
		} TPluginAvatar;
		typedef
			TPluginAvatar* PPluginAvatar;

		typedef struct TPluginMessage
		{
			int cbSize;
			UnicodeString JID;
			double Date;
			int ChatState;
			UnicodeString Body;
			bool Offline;
			UnicodeString DefaultNick;
			bool Store;
			unsigned char Kind;
			bool ShowAsOutgoing;
		} TPluginMessage;
		typedef
			TPluginMessage* PPluginMessage;

		typedef struct TPluginMsgPic
		{
			int cbSize;
			UnicodeString FilePath;
			UnicodeString Description;
			UnicodeString ID;
		} TPluginMsgPic;
		typedef
			TPluginMsgPic* PPluginMsgPic;

		typedef struct TPluginFileTransfer
		{
			int cbSize;
			UnicodeString FilePath;
			UnicodeString Description;
			UnicodeString ID;
		} TPluginFileTransfer;
		typedef
			TPluginFileTransfer* PPluginFileTransfer;

		typedef struct TPluginMicroMsg
		{
			int cbSize;
			UnicodeString Msg;
			bool SaveToArchive;
		} TPluginMicroMsg;
		typedef
			TPluginMicroMsg* PPluginMicroMsg;

		typedef struct TPluginInfo
		{
			int cbSize;
			UnicodeString ShortName;
			DWORD Version;
			UnicodeString Description;
			UnicodeString Author;
			UnicodeString AuthorMail;
			UnicodeString Copyright;
			UnicodeString Homepage;
			unsigned char Flag;
			int ReplaceDefaultModule;
		} TPluginInfo;
		typedef
			TPluginInfo* PPluginInfo;

		typedef struct TPluginActionEdit
		{
			int cbSize;
			UnicodeString pszName;
			UnicodeString Caption;
			UnicodeString Hint;
			bool Enabled;
			bool Visible;
			int IconIndex;
			bool Checked;
		} TPluginActionEdit;
		typedef
			TPluginActionEdit* PPluginActionEdit;

		typedef struct TPluginAccountInfo
		{
			int cbSize;
			UnicodeString Name;
			UnicodeString JID;
			UnicodeString Status;
			int ShowType;
			int IconIndex;
		} TPluginAccountInfo;
		typedef
			TPluginAccountInfo* PPluginAccountInfo;

		typedef struct TPluginWindowStatus
		{
			int cbSize;
			int Status;
			UnicodeString TextStatus;
			bool AllAccounts;
			bool OnlyNote;
		} TPluginWindowStatus;
		typedef
			TPluginWindowStatus* PPluginWindowStatus;

		typedef struct TPluginAction
		{
			int cbSize;
			UnicodeString Action;
			UnicodeString pszName;
			UnicodeString pszCaption;
			DWORD Flags;
			int Position;
			int IconIndex;
			UnicodeString pszService;
			UnicodeString pszPopupName;
			int PopupPosition;
			DWORD hotKey;
			UnicodeString pszContactOwner;
			int GroupIndex;
			bool Grouped;
			bool AutoCheck;
			bool Checked;
			unsigned  int Handle;
			UnicodeString ShortCut;
			UnicodeString Hint;
			UnicodeString PositionAfter;
		} TPluginAction;
		typedef
			TPluginAction* PPluginAction;

		typedef struct TPluginHTTPRequest
		{
			int cbSize;
			unsigned char ProtocolVersion;
			UnicodeString Method;
			UnicodeString URL;
			bool AllowCookies;
			bool ForceEncodeParams;
			bool HandleRedirects;
			UnicodeString HeaderUserAgent;
			UnicodeString HeaderAccept;
			UnicodeString HeaderAcceptLanguage;
			UnicodeString HeaderAcceptEncoding;
			UnicodeString HeaderAcceptCharset;
			UnicodeString HeaderContentType;
			UnicodeString HeaderReferer;
			bool ResultLocation;
			UnicodeString CustomHeaders;
			UnicodeString HTTPPayLoad;
			bool BasicAuth;
			UnicodeString BasicAuthLogin;
			UnicodeString BasicAuthPass;
		} TPluginHTTPRequest;
		typedef
			TPluginHTTPRequest* PPluginHTTPRequest;

		typedef struct TPluginAccountEvents
		{
			int cbSize;
			UnicodeString DisplayName;
			int IconIndex;
			bool EventNew;
			bool EventEdit;
			bool EventDelete;
			bool EventPassChange;
			bool EventDefault;
		} TPluginAccountEvents;
		typedef
			TPluginAccountEvents* PPluginAccountEvents;

		typedef struct TPluginContact
		{
			int cbSize;
			UnicodeString JID;
			UnicodeString Nick;
			UnicodeString Resource;
			UnicodeString Groups;
			int State;
			UnicodeString Status;
			bool Temporary;
			bool FromPlugin;
			int UserIdx;
			unsigned char Subscription;
			bool IsChat;
		} TPluginContact;
		typedef
			TPluginContact* PPluginContact;

		typedef struct TPluginNewsData
		{
			UnicodeString Kind;
			UnicodeString Title;
			UnicodeString ID;
			bool Active;
			int ImageIndex;
		} TPluginNewsData;
		typedef
			TPluginNewsData* PPluginNewsData;

		typedef struct TPluginNewsItem
		{
			double Date;
			UnicodeString News;
			UnicodeString Title;
			UnicodeString Source;
			int ParentIndex;
		} TPluginNewsItem;
		typedef
			TPluginNewsItem* PPluginNewsItem;

		typedef struct TPluginForm
		{
			int cbSize;
			int Handle;
			UnicodeString Name;
			UnicodeString Caption;
			int Width;
			int Height;
		} TPluginForm;
		typedef
			TPluginForm* PPluginForm;

		typedef struct TPluginFont
		{
			bool UseCustomFontSize;
			int CustomFontSize;
			bool UseCustomFontFace;
			UnicodeString CustomFontFace;
			bool UseCustomFontColor;
			int CustomFontColor;
			bool FontBold;
			bool FontUnderLine;
		} TPluginFont;
		typedef
			TPluginFont* PPluginFont;

		typedef struct TPluginRect
		{
			int Left;
			int Top;
			int Right;
			int Bottom;
		} TPluginRect;
		typedef
			TPluginRect* PPluginRect;

		typedef struct TPluginControl
		{
			int cbSize;
			int Handle;
			int FormParentHandle;
			UnicodeString ClassName;
			UnicodeString Name;
			UnicodeString Text;
			UnicodeString Caption;
			TPluginFont Font;
			int ModalResult;
			bool Checked;
			bool Enabled;
			TPluginRect BoundsRect;
			bool AutoSize;
			bool PassField;
		} TPluginControl;
		typedef
			TPluginControl* PPluginControl;

		typedef struct TPluginExecMsg
		{
			UnicodeString JID;
			int UserIdx;
			bool ActionSwitchTo;
			bool ActionCloseWindow;
			bool ActionTabIndex;
			bool ActionTabWasClosed;
			bool IsPriority;
			bool IsFromPlugin;
		} TPluginExecMsg;
		typedef
			TPluginExecMsg* PPluginExecMsg;

		typedef struct TPluginPrivacyItem
		{
			unsigned char ItemType;
			UnicodeString Value;
			unsigned char Msg;
			unsigned char PresIn;
			unsigned char PresOut;
			unsigned char IQ;
		} TPluginPrivacyItem;
		typedef
			TPluginPrivacyItem* PPluginPrivacyItem;

		typedef INT_PTR(__stdcall *TAQQHook)(WPARAM wParam, LPARAM lParam);
		typedef TAQQHook* PAQQHook;

		typedef INT_PTR(__stdcall *TAQQService)(WPARAM wParam, LPARAM lParam);
		typedef TAQQService* PAQQService;

		typedef struct TPluginLink
		{
			wchar_t* Path;
			HANDLE(__stdcall *CreateHookableEvent)(wchar_t* Name);
			INT_PTR(__stdcall *DestroyHookableEvent)(HANDLE hEvent);
			INT_PTR(__stdcall *NotifyEventHooks)(HANDLE hEvent, WPARAM wParam, LPARAM lParam);
			HANDLE(__stdcall *HookEvent)(wchar_t* Name, TAQQHook HookProc);
			HANDLE(__stdcall *HookEventMessage)(wchar_t* Name, HWND Handle, unsigned  int Msg);
			int(__stdcall *UnhookEvent)(HANDLE hHook);
			HANDLE(__stdcall *CreateServiceFunction)(wchar_t* Name, TAQQService ServiceProc);
			void* CreateTransientServiceFunction;
			INT_PTR(__stdcall *DestroyServiceFunction)(HANDLE hService);
			INT_PTR(__stdcall *CallService)(wchar_t* Name, WPARAM wParam, LPARAM lParam);
			INT_PTR(__stdcall *ServiceExists)(wchar_t* Name);
		} TPluginLink;
		typedef
			TPluginLink* PPluginLink;
	}
}

//OTHERS 
#define CALLSERVICE_NOTFOUND 0x80000000
#define MAXMODULELABELLENGTH 64

#define PLUGIN_MAKE_VERSION(a,b,c,d) ((((a)&0xFF)<<24)|(((b)&0xFF)<<16)|(((c)&0xFF)<<8)|((d)&0xFF))
#define PLUGIN_COMPARE_VERSION(a,b) (((DWORD)(a)<(DWORD)(b))?-1:((DWORD)(a)>(DWORD)(b))?1:0)
