/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of SDK for AQQ IM                                         *
 *                                                                             *
 * SDK for AQQ IM is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * SDK is distributed in the hope that it will be useful,                      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#include "ChatUser.h"
#include "AQQ.h"

CChatUser::CChatUser()
{
	this->UserIDx = 0;
	this->JID = "";
	this->RealJID = "";
	this->Affiliation = "";
	this->Role = "";
	this->Offline = false;
	this->Nick = "";
	this->Kicked = false;
	this->Banned = false;
	this->Ver = "";
	this->JIDToShow = "";

	PluginChatPresence = new TPluginChatPresence;
}

CChatUser::~CChatUser()
{
	delete PluginChatPresence;
}

PPluginChatPresence CChatUser::AQQFormat()
{
	ZeroMemory(this->PluginChatPresence, sizeof(TPluginChatPresence));
	this->PluginChatPresence->cbSize = sizeof(TPluginChatPresence);
	this->PluginChatPresence->UserIdx = this->UserIDx;
	this->PluginChatPresence->JID = this->JID.c_str();
	this->PluginChatPresence->RealJID = this->RealJID.c_str();
	this->PluginChatPresence->Affiliation = this->Affiliation.c_str();
	this->PluginChatPresence->Role = this->Role.c_str();
	this->PluginChatPresence->Offline = this->Offline;
	this->PluginChatPresence->Nick = this->Nick.c_str();
	this->PluginChatPresence->Kicked = this->Kicked;
	this->PluginChatPresence->Banned = this->Banned;
	this->PluginChatPresence->Ver = this->Ver.c_str();
	this->PluginChatPresence->JIDToShow = this->JIDToShow.c_str();

	return this->PluginChatPresence;
}

void CChatUser::SDKFormat(PPluginChatPresence PluginChatOpen)
{
	this->UserIDx = PluginChatOpen->UserIdx;
	this->JID = PluginChatOpen->JID;
	this->RealJID = PluginChatOpen->RealJID;
	this->Affiliation = PluginChatOpen->Affiliation;
	this->Role = PluginChatOpen->Role;
	this->Offline = PluginChatOpen->Offline;
	this->Nick = PluginChatOpen->Nick;
	this->Kicked = PluginChatOpen->Kicked;
	this->Banned = PluginChatOpen->Banned;
	this->Ver = PluginChatOpen->Ver;
	this->JIDToShow = PluginChatOpen->JIDToShow;
}

void CChatUser::Add()
{
	this->Offline = false;
	AQQ::System::ChatPresence(this->AQQFormat());
}

void CChatUser::Remove()
{
	this->Offline = true;
	AQQ::System::ChatPresence(this->AQQFormat());
}

void CChatUser::Ban()
{
	this->Banned = true;
	AQQ::System::ChatPresence(this->AQQFormat());
}

void CChatUser::UnBan()
{
	this->Banned = false;
	AQQ::System::ChatPresence(this->AQQFormat());
}

void CChatUser::Kick()
{
	this->Kicked = true;
	AQQ::System::ChatPresence(this->AQQFormat());
}

void CChatUser::Update()
{
	AQQ::System::ChatPresence(this->AQQFormat());
}
