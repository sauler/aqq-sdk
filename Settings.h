/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of SDK for AQQ IM                                         *
 *                                                                             *
 * SDK for AQQ IM is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * SDK is distributed in the hope that it will be useful,                      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#pragma once
#include <vcl.h>
#include <IniFiles.hpp>

class CSettings
{
private:
	TIniFile* SettingsFile;
public:
	CSettings(UnicodeString Filename);
	~CSettings();
	void AddString(UnicodeString Section, UnicodeString SettingName, UnicodeString SettingValue);
	void AddInt(UnicodeString Section, UnicodeString SettingName, int SettingValue);
	void AddFloat(UnicodeString Section, UnicodeString SettingName, float SettingValue);
	void AddBool(UnicodeString Section, UnicodeString SettingName, bool SettingValue);

	UnicodeString GetString(UnicodeString Section, UnicodeString SettingName);
	int GetInt(UnicodeString Section, UnicodeString SettingName);
	float GetFloat(UnicodeString Section, UnicodeString SettingName);
	bool GetBool(UnicodeString Section, UnicodeString SettingName);

	void DeleteSetting(UnicodeString Section, UnicodeString SettingName);
	bool SectionExists(UnicodeString Section);
	void EraseSection(UnicodeString Section);
	void ReadSections(TStringList* Sections);
};
