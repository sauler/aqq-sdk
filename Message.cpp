/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of SDK for AQQ IM                                         *
 *                                                                             *
 * SDK for AQQ IM is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * SDK is distributed in the hope that it will be useful,                      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#include "Message.h"
#include "AQQ.h"

CMessage::CMessage()
{
	this->JID = "";
	this->Date = 0;
	this->ChatState = CHAT_NONE;
	this->Body = "";
	this->Offline = false;
	this->DefaultNick = "";
	this->Store = false;
	this->Kind = MSGKIND_CHAT;
	this->ShowAsOutgoing = false;

	PluginMessage = new TPluginMessage;
}

CMessage::~CMessage()
{
	delete PluginMessage;
}

PPluginMessage CMessage::AQQFormat()
{
	ZeroMemory(this->PluginMessage, sizeof(TPluginMessage));
	this->PluginMessage->cbSize = sizeof(TPluginMessage);
	this->PluginMessage->JID = this->JID.c_str();
	this->PluginMessage->Date = this->Date;
	this->PluginMessage->ChatState = this->ChatState;
	this->PluginMessage->Body = this->Body.c_str();
	this->PluginMessage->Offline = this->Offline;
	this->PluginMessage->DefaultNick = this->DefaultNick.c_str();
	this->PluginMessage->Store = this->Store;
	this->PluginMessage->Kind = this->Kind;
	this->PluginMessage->ShowAsOutgoing = this->ShowAsOutgoing;
	return this->PluginMessage;
}

void CMessage::SDKFormat(PPluginMessage PluginMessage)
{
	this->JID = PluginMessage->JID;
	this->Date = PluginMessage->Date;
	this->ChatState = PluginMessage->ChatState;
	this->Body = PluginMessage->Body;
	this->Offline = PluginMessage->Offline;
	this->DefaultNick = PluginMessage->DefaultNick;
	this->Store = PluginMessage->Store;
	this->Kind = PluginMessage->Kind;
	this->ShowAsOutgoing = PluginMessage->ShowAsOutgoing;
}

void CMessage::Notify()
{
	AQQ::Contacts::Message(this->AQQFormat());
}

int CMessage::Send(CContact *Contact)
{
	return AQQ::Contacts::SendMessage(Contact->AQQFormat(), this->AQQFormat());
}

int CMessage::SendImage(CContact *Contact, UnicodeString Filename)
{
	return AQQ::Contacts::SendImage(Contact->AQQFormat(),Filename);
}
