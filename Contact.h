/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of SDK for AQQ IM                                         *
 *                                                                             *
 * SDK for AQQ IM is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * SDK is distributed in the hope that it will be useful,                      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#pragma once
#include <vcl.h>
#include "ContactInfo.h"
#include "AQQ.h"
#include "Avatar.h"
#include "Status.h"
#include "Settings.h"

class CAccount;

class CContact {
private:
	PPluginContact PluginContact;
protected:
	CSettings* Settings;
public:
	CContact();
	CContact(UnicodeString AccountName);
	CContact(UnicodeString AccountName, UnicodeString JID);
	virtual ~CContact();

	PPluginContact AQQFormat();
	void SDKFormat(PPluginContact pluginContact);

	CAvatar *Avatar;
	CContactInfo *ContactInfo;
	UnicodeString JID;
	UnicodeString Nick;
	UnicodeString Resource;
	UnicodeString Groups;
	CStatus Status;
	bool Temporary;
	bool FromPlugin;
	int UserIDx;
	byte Subscription;
	bool Chat;

	UnicodeString AccountName;

	void Create();
	void Delete();
	void Update();
	void Ban();
	bool HaveBan();
	void RemoveBan();

	CStatus GetStatus();
	void SetStatus(int State, UnicodeString Description);

	void SaveSettings();
	void LoadSettings();
	void EraseSettings();
};

