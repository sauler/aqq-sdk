/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of SDK for AQQ IM                                         *
 *                                                                             *
 * SDK for AQQ IM is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * SDK is distributed in the hope that it will be useful,                      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#pragma once
#include "PluginAPI.h"
#include <vcl.h>



namespace AQQ {
	namespace System {
		int AccountEvents(int ID, PPluginAccountEvents PluginAccountEvents);
		void AddImpExp(PPluginImpExp PluginImpExp);
		int AddSMSGate(PPluginSMSGate PluginSMSGate);
		UnicodeString AppVer();
		void ChangeJabberResources(int Flag, UnicodeString NewResourceName);
		void ChangeSMSID(UnicodeString  OldID, UnicodeString  NewID);
		void ChangeSMSStatus(int SMSStateConst, UnicodeString  SMSID);
		void Chat(PPluginChatPrep PluginChatPrep);
		void ChatOpen(PPluginChatOpen PluginChatOpen);
		void ChatPresence(PPluginChatPresence PluginChatPresence);
		void ColorChange(int Hue, int Saturation, int Brightness);
		int GetBrightness();
		int GetHue();
		int GetSaturation();
		void DebugXML(PPluginDebugInfo PluginDebugInfo);
		int Error(PPluginError PluginError);
		void SetEnabled(int FunctionID, int Enable);
		UnicodeString  GetNewCacheItemPatch(UnicodeString Filename, UnicodeString ContactJID);
		void InterpretXML(int AccountID, UnicodeString XML);
		bool ModulesLoaded();
		int MsgComposing(PPluginContact PluginContact, PPluginChatState PluginChatState);
		void NewsSourceAdd(PPluginNewsData PluginNewsData, int AccountID);
		int NewsSourceDelete(PPluginNewsData PluginNewsData);
		int NewsSourceFetchEnd(int ID, int State);
		void NewsSourceFetchStart(int ID);
		void NewsSourceItem(PPluginNewsItem PluginNewsItem);
		int NewsSourceRefresh(PPluginNewsData PluginNewsData);
		int NewsSourceUpdate(PPluginNewsData PluginNewsData);
		void OnConnectSilence();
		void PlaySound(int SoundID, int Flag);
		void PlaySound(UnicodeString Filename, int Flag);
		int PluginActive(int Mode, UnicodeString Filename);
		int PluginExclude(int Mode, UnicodeString Filename);
		void RefreshPlugins(int Mode);
		void RefreshPlugin(UnicodeString Filename);
		int RegExp(UnicodeString Regexp, int Mode);
		int RemoveAgent(UnicodeString JID);
		int RemoveSMSGate(int ID);
		int RenameSMSGate(UnicodeString NewName, int ID);
		void Restart(int Mode);
		int RunAction(UnicodeString Action);
		void SendHook(PPluginHook PluginHook);
		int SendXML(UnicodeString XML, int AccountID);
		int SetAgent(PPluginAgent PluginAgent);
		void SetShowAndStatus(PPluginStateChange PluginStateChange);
		int SocialSend(int SocialMedia, UnicodeString Message);
		void ApplyTheme(int Mode);
		void RefreshTheme();
		void SetTheme(UnicodeString Path);
		void TooltipAddItem(PPluginToolTipItem PluginToolTipItem);
		void TooltipShow(int Delay);
		void ChangeTransferStatus(PPluginTransfer PluginTransfer);
		void RefreshTrayIcon();
	}

	namespace Controls {
		void CreateButton(PPluginAction PluginAction);
		void CreatePopupMenu(PPluginAction PluginAction);
		int CreatePopupMenuItem(PPluginAction PluginAction);
		void DestroyButton(PPluginAction PluginAction);
		void DestroyPopupMenu(PPluginAction PluginAction);
		void DestroyPopupMenuItem(PPluginAction PluginAction);
		void EditPopupMenuItem(PPluginActionEdit PluginActionEdit);
		PPluginAction GetPopupMenuItem(PPluginItemDescriber PluginItemDescriber);
		void SetMainStatusPanelText(UnicodeString NewStatus);
		void SMSControlsEnable(int Enable);
		void UpdateButton(PPluginAction PluginAction);
		int WebBrowserClickID(PPluginWebItem PluginWebItem, PPluginWebBrowser PluginWebBrowser);
		int WebBrowserCreate(int Handle, PPluginWebBrowser PluginWebBrowser);
		int WebBrowserDestroy(PPluginWebBrowser PluginWebBrowser);
		void WebBrowserGetID(PPluginWebItem PluginWebItem, PPluginWebBrowser PluginWebBrowser);
		int WebBrowserNavigate(UnicodeString URL, PPluginWebBrowser PluginWebBrowser);
		int WebBrowserSetID(PPluginWebItem PluginWebItem, PPluginWebBrowser PluginWebBrowser);
	}

	namespace Contacts {
		void AckMsg(int AckID, int Status);
		void AddBan(UnicodeString ContactJID);
		void AddForm(PPluginAddForm PluginAddForm);
		int AddLineInfo(PPluginContact Contact, UnicodeString Message, bool SaveToArchive);
		void CloseTab(int Mode, UnicodeString ContactJID);
		void FetchAllTabs();
		void SetTabCaption(UnicodeString Name, PPluginContact PluginContact);
		void SetTabIcon(int IconID, PPluginContact PluginContact);
		void Create(PPluginContact PluginContact);
		void Delete(PPluginContact PluginContact);
		void DestroyDomain(UnicodeString Domain);
		void SearchXMLError(UnicodeString XML);
		int FillSimpleInfo(PPluginContactSimpleInfo PluginContactSimpleInfo);
		UnicodeString GetHTMLStatus(PPluginContact PluginContact);
		bool HaveBan(UnicodeString ContactJID);
		void Message(PPluginMessage PluginMessage);
		void RemoveBan(UnicodeString ContactJID);
		int RequestBuddy(int AccountID, UnicodeString ContactJID);
		void RequestList();
		void SearchXMLResult(UnicodeString XML);
		void VCardResult(UnicodeString SessionID, UnicodeString XML);
		bool SendMessage(PPluginContact PluginContact, PPluginMessage PluginMessage);
		bool SendImage(PPluginContact PluginContact, UnicodeString Filename);
		bool SetAvatar(PPluginAvatar PluginAvatar, PPluginContact PluginContact);
		void SetSimpleInfo(PPluginContactSimpleInfo PluginContactSimpleInfo);
		void SetHTMLStatus(PPluginContact PluginContact, UnicodeString HTMLStatus);
		void SetOffline(UnicodeString Domain);
		void SetWebAvatar(UnicodeString URL, PPluginContact PluginContact);
		void Update(PPluginContact PluginContact);
		UnicodeString GetStatePNGFilePath(PPluginContact PluginContact);
		int GetStatePNGIndex(PPluginContact PluginContact);
	}

	namespace Icons {
		void DestroyIcon(int IconID);
		int LoadIcon(UnicodeString Filename);
		int ReplaceIcon(int IconID, UnicodeString Filename);
		UnicodeString GetPNGHDFilePath(int IconID);
		UnicodeString GetPathFromID(int IconID);
	}

	namespace Paths {
		UnicodeString GetAppFilePath();
		UnicodeString GetAppPath();
		UnicodeString GetPluginDir(int Handle);
		UnicodeString GetPluginUserDir();
		UnicodeString GetThemeDir();
		UnicodeString GetThemeDirRW();
		UnicodeString GetUserDir();
		void ForceDirectories(UnicodeString Directory);
	}

	namespace Functions {
		void Log(int Mode, UnicodeString Message);
		PPluginProxy GetProxy();
		int ShowMessage(int Type, UnicodeString Message);
		bool IsListReady();
		void ExtractResource(UnicodeString Filename, UnicodeString ResourceName, UnicodeString ResourceType);
		UnicodeString FetchSettings();
	}
}
