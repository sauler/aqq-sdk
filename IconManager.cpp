/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of SDK for AQQ IM                                         *
 *                                                                             *
 * SDK for AQQ IM is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * SDK is distributed in the hope that it will be useful,                      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#include "AQQ.h"
#include "IconManager.h"

CIconManager * CIconManager::Instance = 0;

CIconManager::~CIconManager()
{
	DestroyAllIcons();
}

CIconManager * CIconManager::instance()
{
	if (Instance == 0)
		Instance = new CIconManager();
	return Instance;
}

void CIconManager::LoadIcon(UnicodeString Name, UnicodeString Filename)
{
	Icons[Name] = AQQ::Icons::LoadIcon(Filename);
}

void CIconManager::DestroyIcon(UnicodeString Name)
{
	AQQ::Icons::DestroyIcon(Icons[Name]);
	Icons.erase(Name);
}

void CIconManager::ReplaceIcon(UnicodeString Name, UnicodeString Filename)
{
	Icons[Name] = AQQ::Icons::ReplaceIcon(Icons[Name], Filename);
}

int CIconManager::GetIconByName(UnicodeString Name)
{
	return Icons[Name];
}

UnicodeString CIconManager::GetIconPathByName(UnicodeString Name)
{
	return AQQ::Icons::GetPathFromID(Icons[Name]);
}

void CIconManager::DestroyAllIcons()
{
	for (Icons_it iterator = Icons.begin(); iterator != Icons.end(); iterator++) {
		this->DestroyIcon(iterator->second);
	}
	Icons.clear();
}
