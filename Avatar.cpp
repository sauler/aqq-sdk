/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of SDK for AQQ IM                                         *
 *                                                                             *
 * SDK for AQQ IM is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * SDK is distributed in the hope that it will be useful,                      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#include "Avatar.h"
#include "AQQ.h"
#include "Contact.h"

CAvatar::CAvatar(CContact *Contact)
{
	this->Filename = "";
	this->XEPEmpty = false;
	this->SilientMode = false;
	this->AccountJID = "";

	PluginAvatar = new TPluginAvatar;
	this->Contact = Contact;
}

CAvatar::~CAvatar()
{
	delete PluginAvatar;
}

PPluginAvatar CAvatar::AQQFormat()
{
	ZeroMemory(this->PluginAvatar, sizeof(TPluginAvatar));
	this->PluginAvatar->FileName = this->Filename.c_str();
	this->PluginAvatar->XEPEmpty = this->XEPEmpty;
	this->PluginAvatar->SilentMode = this->SilientMode;
	this->PluginAvatar->JID = this->AccountJID.c_str();
	return this->PluginAvatar;
}

void CAvatar::SDKFormat(PPluginAvatar PluginAvatar)
{
	this->Filename = PluginAvatar->FileName;
	this->XEPEmpty = PluginAvatar->XEPEmpty;
	this->SilientMode = PluginAvatar->SilentMode;
	this->AccountJID = PluginAvatar->JID;
}

int CAvatar::SetAvatar()
{
	return AQQ::Contacts::SetAvatar(this->AQQFormat(), this->Contact->AQQFormat());
}

void CAvatar::SetWebAvatar(UnicodeString URL)
{
	AQQ::Contacts::SetWebAvatar(URL, this->Contact->AQQFormat());
}
