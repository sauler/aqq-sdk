/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of SDK for AQQ IM                                         *
 *                                                                             *
 * SDK for AQQ IM is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * SDK is distributed in the hope that it will be useful,                      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#pragma once
#include "Contact.h"
#include <map>
#include <vcl.h>

class CContactManager
{
private:
	typedef std::map<UnicodeString,CContact*>::iterator Contacts_it;
	std::map<UnicodeString,CContact*>Contacts;
	UnicodeString LastAddedContact;

public:
	CContactManager();
	~CContactManager();

	void AddContact(CContact* Contact);
	void RemoveContact(UnicodeString JID);
	CContact* GetContactByJID(UnicodeString JID);
	CContact* GetLastAddedContact();
	void DeleteAllContacts();

};
