/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of SDK for AQQ IM                                         *
 *                                                                             *
 * SDK for AQQ IM is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * SDK is distributed in the hope that it will be useful,                      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#include "Chat.h"
#include "AQQ.h"

CChat::CChat()
{
	PluginChatOpen = new TPluginChatOpen;
}

CChat::~CChat()
{
	delete PluginChatOpen;
}

PPluginChatOpen CChat::AQQFormat()
{
	ZeroMemory(this->PluginChatOpen,sizeof(TPluginChatOpen));
	this->PluginChatOpen->cbSize = sizeof(TPluginChatOpen);
	this->PluginChatOpen->JID = this->JID.c_str();
	this->PluginChatOpen->UserIdx = this->UserIDx;
	this->PluginChatOpen->Channel = this->ChannelName.c_str();
	this->PluginChatOpen->Nick = this->Nick.c_str();
	this->PluginChatOpen->NewWindow = this->NewWindow;
	this->PluginChatOpen->IsNewMsg = this->IsNewMsg;
	this->PluginChatOpen->Priority = this->Priority;
	this->PluginChatOpen->OriginJID = this->OriginJID.c_str();
	this->PluginChatOpen->ImageIndex = this->ImageIndex;
	this->PluginChatOpen->AutoAccept = this->AutoAccept;
	this->PluginChatOpen->ChatMode = this->ChatMode;

	return this->PluginChatOpen;
}

void CChat::SDKFormat(PPluginChatOpen PluginChatOpen)
{
	this->JID = PluginChatOpen->JID;
	this->UserIDx = PluginChatOpen->UserIdx;
	this->ChannelName = PluginChatOpen->Channel;
	this->Nick = PluginChatOpen->Nick;
	this->NewWindow = PluginChatOpen->NewWindow;
	this->IsNewMsg = PluginChatOpen->IsNewMsg;
	this->Priority = PluginChatOpen->Priority;
	this->OriginJID = PluginChatOpen->OriginJID;
	this->ImageIndex = PluginChatOpen->ImageIndex;
	this->AutoAccept = PluginChatOpen->AutoAccept;
	this->ChatMode = PluginChatOpen->ChatMode;
}

void CChat::Open()
{
    AQQ::System::ChatOpen(this->AQQFormat());
}
