/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of SDK for AQQ IM                                         *
 *                                                                             *
 * SDK for AQQ IM is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * SDK is distributed in the hope that it will be useful,                      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#pragma once
#include <System.Classes.hpp>

#include "ChatUser.h"
#include "Contact.h"
#include "Message.h"
#include "StateButton.h"

class CSynchronizer: public TObject
{
private:
	HWND FWindowHandle;
	TNotifyEvent MessageEvent;
	TNotifyEvent ContactEvent;
	TNotifyEvent ChatUserEvent;
	TNotifyEvent LineInfoEvent;
	TNotifyEvent ButtonEvent;

	CContact* Contact;
	CMessage* Message;
	CChatUser* ChatUser;
	CStateButton* StateButton;
	UnicodeString LineInfoMessage;
protected:
	void __fastcall WndProc(TMessage & Message);
	void __fastcall SendMessageToAQQ(TObject *Sender);
	void __fastcall UpdateAQQContact(TObject *Sender);
	void __fastcall UpdateAQQChatUser(TObject *Sender);
	void __fastcall AddLineInfoToChat(TObject *Sender);
	void __fastcall UpdateAQQStateButton(TObject *Sender);
public:
	CSynchronizer();
	__fastcall ~CSynchronizer();
	void NotifyMessage(CMessage* Message);
	void UpdateContact(CContact* Contact);
	void UpdateChatUser(CChatUser* ChatUser);
	void AddLineInfo(CContact* Contact, UnicodeString Message);
	void UpdateStateButton(CStateButton* StateButton);
	__property TNotifyEvent OnAQQMessage  = {read=MessageEvent, write=MessageEvent, default=0};
	__property TNotifyEvent OnAQQContactUpdate  = {read=ContactEvent, write=ContactEvent, default=0};
	__property TNotifyEvent OnAQQChatUserUpdate  = {read=ChatUserEvent, write=ChatUserEvent, default=0};
	__property TNotifyEvent OnAQQAddLineInfo  = {read=LineInfoEvent, write=LineInfoEvent, default=0};
	__property TNotifyEvent OnAQQStateButtonUpdate  = {read=ButtonEvent, write=ButtonEvent, default=0};
};
