/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of SDK for AQQ IM                                         *
 *                                                                             *
 * SDK for AQQ IM is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * SDK is distributed in the hope that it will be useful,                      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#pragma once
#include <vcl.h>
#include "PluginAPI.h"

class CSynchronizer;

class CStateButton
{
private:
    PPluginAction PluginAction;
	TTimer *BlinkTimer;
	CSynchronizer* Synchronizer;
	void __fastcall BlinkTimerEvent(TObject *Sender);

	int BlinkActualIcon;
	int Timeout;
	int Elapsed;
	bool UseTimeout;
	bool Stop;
public:
	int BlinkStartIcon;
	int BlinkStopIcon;

	CStateButton();
	~CStateButton();

	PPluginAction AQQFormat();
	void SDKFormat(PPluginAction PluginAction);

	void CreateButton();
	void CreateButton(UnicodeString Name, int Position, int Icon, UnicodeString ServiceFunction, UnicodeString PopupName);
	void CreateButton(UnicodeString Name, UnicodeString positionAfter, int Icon, UnicodeString ServiceFunction, UnicodeString PopupName);
	void DestroyButton();
	void UpdateNS();
	void Update();

	UnicodeString Action;
	UnicodeString Name;
	UnicodeString Caption;
	int Position;
	int IconID;
	UnicodeString Service;
	UnicodeString PopupName;
	int PopupPosition;
	int GroupIndex;
	bool Grouped;
	bool AutoCheck;
	bool Checked;
	unsigned int Handle;
	UnicodeString Shortcut;
	UnicodeString Hint;
	UnicodeString PositionAfter;

	void Blink();
	void Blink(int Timeout);
	void StopBlink();
};

