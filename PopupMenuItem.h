/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of SDK for AQQ IM                                         *
 *                                                                             *
 * SDK for AQQ IM is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * SDK is distributed in the hope that it will be useful,                      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/
#pragma once
#include <vcl.h>
#include "PluginAPI.h"

class CPopupMenuItem
{
private:
	PPluginAction PluginAction;
	PPluginActionEdit PluginActionEdit;
public:
	CPopupMenuItem();
	CPopupMenuItem(UnicodeString Name, UnicodeString Caption, int Position, int IconID, UnicodeString Service, UnicodeString PopupName = "");
	CPopupMenuItem(UnicodeString Name, UnicodeString Caption, UnicodeString PositionAfter, int IconID, UnicodeString Service, UnicodeString PopupName = "");
	~CPopupMenuItem();
	PPluginAction AQQFormat();
	void SDKFormat(PPluginAction PluginAction);

	int Create();
	void Destroy();
	void Get(PPluginItemDescriber PluginItemDescriber);
	void Update();

	UnicodeString Action;
	UnicodeString Name;
	UnicodeString Caption;
	int Position;
	int IconID;
	UnicodeString Service;
	UnicodeString PopupName;
	int PopupPosition;
	int GroupIndex;
	bool Grouped;
	bool AutoCheck;
	bool Checked;
	unsigned int Handle;
	UnicodeString Shortcut;
	UnicodeString Hint;
	UnicodeString PositionAfter;

	bool Enabled;
	bool Visible;
};

