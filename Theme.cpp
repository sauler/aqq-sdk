/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of SDK for AQQ IM                                         *
 *                                                                             *
 * SDK for AQQ IM is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * SDK is distributed in the hope that it will be useful,                      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#include "Theme.h"
#include <inifiles.hpp>

#include "AQQ.h"
#include "PluginLink.h"
#include "Paths.h"

CTheme* CTheme::Instance = 0;

CTheme::CTheme()
{
	CPluginLink::instance()->GetLink().HookEvent(AQQ_SYSTEM_THEMECHANGED, ThemeChanged);
	CPluginLink::instance()->GetLink().HookEvent(AQQ_SYSTEM_THEMEINFO, ThemeInfo);
	CPluginLink::instance()->GetLink().HookEvent(AQQ_SYSTEM_THEMESTART, ThemeStart);
	ActiveTheme = CPaths::instance()->ThemeDir();
}

CTheme::~CTheme()
{
	CPluginLink::instance()->GetLink().UnhookEvent(ThemeChanged);
	CPluginLink::instance()->GetLink().UnhookEvent(ThemeInfo);
	CPluginLink::instance()->GetLink().UnhookEvent(ThemeStart);
}

CTheme* CTheme::instance()
{

	return Instance;
}

void CTheme::InitInstance()
{
	Instance = new CTheme();
}

void CTheme::ResetInstance()
{
    delete Instance;
	Instance = NULL;
}

bool CTheme::SkinEnabled()
{
	TStrings* IniList = new TStringList();
	IniList->SetText(AQQ::Functions::FetchSettings().w_str());
	TMemIniFile* Settings = new TMemIniFile(ChangeFileExt(Application->ExeName, ".INI"));
	Settings->SetStrings(IniList);
	delete IniList;
	UnicodeString SkinsEnabled = Settings->ReadString("Settings","UseSkin","1");
	delete Settings;
	return StrToBool(SkinsEnabled);
}

bool CTheme::AnimateWindows()
{
	TStrings* IniList = new TStringList();
	IniList->SetText(AQQ::Functions::FetchSettings().w_str());
	TMemIniFile* Settings = new TMemIniFile(ChangeFileExt(Application->ExeName, ".INI"));
	Settings->SetStrings(IniList);
	delete IniList;
	UnicodeString AnimateWindowsEnabled = Settings->ReadString("Theme","ThemeAnimateWindows","1");
	delete Settings;
	return StrToBool(AnimateWindowsEnabled);
}

bool CTheme::Glowing()
{
	TStrings* IniList = new TStringList();
	IniList->SetText(AQQ::Functions::FetchSettings().w_str());
	TMemIniFile* Settings = new TMemIniFile(ChangeFileExt(Application->ExeName, ".INI"));
	Settings->SetStrings(IniList);
	delete IniList;
	UnicodeString GlowingEnabled = Settings->ReadString("Theme","ThemeGlowing","1");
	delete Settings;
	return StrToBool(GlowingEnabled);
}

void CTheme::Apply(int Mode)
{
	AQQ::System::ApplyTheme(Mode);
}

void CTheme::Refresh()
{
	AQQ::System::RefreshTheme();
}

void CTheme::Set(UnicodeString Directory)
{
	AQQ::System::SetTheme(Directory);
}

void CTheme::ChangeColor(int Hue, int Saturation, int Brightness)
{
	AQQ::System::ColorChange(Hue, Saturation, Brightness);
}

int CTheme::Hue()
{
	return AQQ::System::GetHue();
}

int CTheme::Saturation()
{
	return AQQ::System::GetSaturation();
}

int CTheme::Brightness()
{
	return AQQ::System::GetBrightness();
}

UnicodeString CTheme::GetActiveTheme()
{
	return ActiveTheme;
}

INT_PTR __stdcall CTheme::ThemeChanged(WPARAM wParam, LPARAM lParam)
{
	CTheme::Instance->ActiveTheme = (wchar_t*)lParam;
}

INT_PTR __stdcall CTheme::ThemeInfo(WPARAM wParam, LPARAM lParam)
{
	CTheme::Instance->ActiveTheme = (wchar_t*)lParam;
}

INT_PTR __stdcall CTheme::ThemeStart(WPARAM wParam, LPARAM lParam)
{
	CTheme::Instance->ActiveTheme = (wchar_t*)lParam;
}

