/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of SDK for AQQ IM                                         *
 *                                                                             *
 * SDK for AQQ IM is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * SDK is distributed in the hope that it will be useful,                      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#pragma once
#include <vcl.h>
#include "PluginAPI.h"
#include "Contact.h"

class CMessage
{
private:
	PPluginMessage PluginMessage;
public:
	CMessage();
	~CMessage();
	PPluginMessage AQQFormat();
	void SDKFormat(PPluginMessage PluginMessage);

	UnicodeString JID;
	double Date;
	int ChatState;
	UnicodeString Body;
	bool Offline;
	UnicodeString DefaultNick;
	bool Store;
	byte Kind;
	bool ShowAsOutgoing;

	void Notify();
	int Send(CContact *Contact);
	int SendImage(CContact *Contact, UnicodeString Filename);
};

