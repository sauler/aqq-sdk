/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of SDK for AQQ IM                                         *
 *                                                                             *
 * SDK for AQQ IM is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * SDK is distributed in the hope that it will be useful,                      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#include "Synchronizer.h"
#include <windows.h>

#define WM_AQQMESSAGE WM_USER+1995
#define WM_AQQCONTACT_UPDATE WM_USER+1996
#define WM_AQQCHAT_USER_UPDATE WM_USER+1997
#define WM_AQQADD_LINE_INFO WM_USER+1998
#define WM_AQQBUTTON_UPDATE WM_USER+1999

CSynchronizer::CSynchronizer()
{
	FWindowHandle = 0;
	FWindowHandle = AllocateHWnd(&WndProc);
	this->OnAQQMessage = &SendMessageToAQQ;
	this->OnAQQContactUpdate = &UpdateAQQContact;
	this->OnAQQChatUserUpdate = &UpdateAQQChatUser;
	this->OnAQQAddLineInfo = &AddLineInfoToChat;
	this->OnAQQStateButtonUpdate = &UpdateAQQStateButton;
}

__fastcall CSynchronizer::~CSynchronizer()
{
    if (FWindowHandle)
		DeallocateHWnd(FWindowHandle);
}

void __fastcall CSynchronizer::WndProc(TMessage & Message)
{
	TNotifyEvent event;
	event = 0;
	switch (Message.Msg)
	{
		case WM_AQQMESSAGE:
			event = OnAQQMessage;
			break;
		case WM_AQQCONTACT_UPDATE:
			event = OnAQQContactUpdate;
			break;
		case WM_AQQCHAT_USER_UPDATE:
			event = OnAQQChatUserUpdate;
			break;
		case WM_AQQADD_LINE_INFO:
			event = OnAQQAddLineInfo;
			break;
		case WM_AQQBUTTON_UPDATE:
			event = OnAQQStateButtonUpdate;
			break;
		default:
			Dispatch(&Message);
			return;
	}

	if (event)
		event(this);
}

void CSynchronizer::NotifyMessage(CMessage* Message)
{
	this->Message = Message;
	SendMessage(FWindowHandle, WM_AQQMESSAGE, 0, 0);
}

void __fastcall CSynchronizer::SendMessageToAQQ(TObject *Sender)
{
	Message->Notify();
	delete Message;
}

void CSynchronizer::UpdateContact(CContact* Contact)
{
	this->Contact = Contact;
	SendMessage(FWindowHandle, WM_AQQCONTACT_UPDATE, 0, 0);
}

void __fastcall CSynchronizer::UpdateAQQContact(TObject *Sender)
{
	Contact->Update();
}

void CSynchronizer::UpdateChatUser(CChatUser* ChatUser)
{
	this->ChatUser = ChatUser;
	SendMessage(FWindowHandle, WM_AQQCHAT_USER_UPDATE, 0, 0);
}

void __fastcall CSynchronizer::UpdateAQQChatUser(TObject *Sender)
{
	ChatUser->Update();
	delete ChatUser;
}

void CSynchronizer::AddLineInfo(CContact* Contact, UnicodeString Message)
{
	this->Contact = Contact;
	this->LineInfoMessage = Message;
	SendMessage(FWindowHandle, WM_AQQADD_LINE_INFO, 0, 0);
}

void __fastcall CSynchronizer::AddLineInfoToChat(TObject *Sender)
{
	if (Contact != NULL)
		AQQ::Contacts::AddLineInfo(Contact->AQQFormat(), this->LineInfoMessage, true);

}

void CSynchronizer::UpdateStateButton(CStateButton* StateButton)
{
	this->StateButton = StateButton;
	SendMessage(FWindowHandle, WM_AQQBUTTON_UPDATE, 0, 0);
}

void __fastcall CSynchronizer::UpdateAQQStateButton(TObject *Sender)
{
	if (StateButton != NULL)
		StateButton->UpdateNS();

}
