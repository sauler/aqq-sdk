/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of SDK for AQQ IM                                         *
 *                                                                             *
 * SDK for AQQ IM is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * SDK is distributed in the hope that it will be useful,                      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#pragma once
#include <vcl.h>
#include "PluginAPI.h"
#include "AccountDetails.h"
#include "Settings.h"

class CAccount
{
private:
	PPluginAccountEvents PluginAccountEvents;
protected:
	CAccountDetails *Details;
	CSettings* Settings;
	int AccountID;
public:
	CAccount();
	virtual ~CAccount();

	PPluginAccountEvents AQQFormat();
	void SDKFormat(PPluginAccountEvents PluginAccountEvents);
	UnicodeString Name;
	int IconID;
	bool CanCreate;
	bool CanEdit;
	bool CanDelete;
	bool CanChangePass;
	bool DefaultEvent;

	void Create();
	void Update();
	void Delete();

	virtual void OnDefaultEvent() = 0;
	virtual void OnNewEvent() = 0;
	virtual void OnEditEvent() = 0;
	virtual void OnDeleteEvent() = 0;
	virtual void OnChangePassEvent() = 0;


	int GetAccountID();
	CAccountDetails *GetDetails();

	void SaveSettings();
	void LoadSettings();
	void EraseSettings();
};

