# SDK dla komunikatora AQQ #

Nakładka na [PluginAPI](https://bitbucket.org/beherit/pluginapi-for-aqq-im), ułatwiająca pisanie wtyczek dla komunikatora [AQQ](http://aqq.eu/).

### Wymagania ###

Do użycia tego SDK potrzebujesz:

* C++ Builder
* [PluginAPI](https://bitbucket.org/beherit/pluginapi-for-aqq-im)

### Dokumentacja ###

Dokumentację tego SDK znajdziesz po lewej stronie w zakładce Documentation lub na oficjalnej [stronie dokumentacji](https://sauler.gitbooks.io/aqq-sdk/content/index.html).

### Kontakt z autorem ###

Autorem tego SDK jest Rafał Babiarz. Możesz skontaktować się z nim drogą mailową (sauler1995@gmail.com) lub poprzez Jabber (sauler@jix.im).

### Licencja ###

SDK objęte jest licencją [GNU General Public License 3](http://www.gnu.org/copyleft/gpl.html).

```
#!

SDK dla komunikatora AQQ
Copyright © 2016  Rafał Babiarz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
```