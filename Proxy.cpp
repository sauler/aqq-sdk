/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of SDK for AQQ IM                                         *
 *                                                                             *
 * SDK for AQQ IM is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * SDK is distributed in the hope that it will be useful,                      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#include "Proxy.h"
#include "AQQ.h"


CProxy::CProxy()
{
	this->Enabled = false;
	this->Server = "";
	this->Port = 0;
	this->Type = 0;
	this->Auth = false;
	this->Login = "";
	this->Password = "";

	PluginProxy = new TPluginProxy;
}


CProxy::~CProxy()
{
	delete PluginProxy;
}

PPluginProxy CProxy::AQQFormat()
{
	ZeroMemory(this->PluginProxy, sizeof(TPluginProxy));
	this->PluginProxy->cbSize = sizeof(TPluginProxy);
	this->PluginProxy->ProxyEnabled = this->Enabled;
	this->PluginProxy->ProxyServer = this->Server.c_str();
	this->PluginProxy->ProxyPort = this->Port;
	this->PluginProxy->ProxyType = this->Type;
	this->PluginProxy->ProxyAuth = this->Auth;
	this->PluginProxy->ProxyLogin = this->Login.c_str();
	this->PluginProxy->ProxyPass = this->Password.c_str();
	return this->PluginProxy;
}

void CProxy::SDKFormat(PPluginProxy PluginProxy)
{
	this->Enabled = PluginProxy->ProxyEnabled;
	this->Server = PluginProxy->ProxyServer;
	this->Port = PluginProxy->ProxyPort;
	this->Type = PluginProxy->ProxyType;
	this->Auth = PluginProxy->ProxyAuth;
	this->Login = PluginProxy->ProxyLogin;
	this->Password = PluginProxy->ProxyPass;
}

void CProxy::Get()
{
	this->SDKFormat(AQQ::Functions::GetProxy());
}
