/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of SDK for AQQ IM                                         *
 *                                                                             *
 * SDK for AQQ IM is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * SDK is distributed in the hope that it will be useful,                      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#pragma once
#include <vcl.h>
#include "PluginAPI.h"

class CProtocol
{
private:
	PPluginAgent PluginAgent;
	TPluginMaxStatus PluginMaxStatus;

	// AQQ Hook
	static INT_PTR __stdcall MaxMessageLengthHook(WPARAM wParam, LPARAM lParam);
	static INT_PTR __stdcall MaxStatusLengthHook(WPARAM wParam, LPARAM lParam);
	static INT_PTR __stdcall ShowTypePathHook(WPARAM wParam, LPARAM lParam);
	static INT_PTR __stdcall ClipboardHook(WPARAM wParam, LPARAM lParam);
	static CProtocol* Instance;
public:
	CProtocol();
	~CProtocol();
	PPluginAgent AQQFormat();
	void SDKFormat(PPluginAgent PluginAgent);
	UnicodeString JID;
	UnicodeString Name;
	UnicodeString Prompt;
	bool Transport;
	bool Search;
	bool GroupChat;
	UnicodeString Description;
	bool RequiredID;
	int IconID;
	UnicodeString AccountName;

	int Register();
	int Update();
	bool Remove();

	virtual int MaxMessageLength() = 0;
	virtual int MaxStatusLength() = 0;
	virtual bool CheckContact(UnicodeString JID) = 0;
	virtual UnicodeString Clipboard(UnicodeString JID) = 0;
	virtual UnicodeString OnlineIconPath() = 0;
	virtual UnicodeString FFCIconPath() = 0;
	virtual UnicodeString AwayIconPath() = 0;
	virtual UnicodeString NAIconPath() = 0;
	virtual UnicodeString DNDIconPath() = 0;
	virtual UnicodeString InvisibleIconPath() = 0;
	virtual UnicodeString OfflineIconPath() = 0;
	virtual UnicodeString BlockedIconPath() = 0;
	virtual int OnlineIcon() = 0;
	virtual int FFCIcon() = 0;
	virtual int AwayIcon() = 0;
	virtual int NAIcon() = 0;
	virtual int DNDIcon() = 0;
	virtual int InvisibleIcon() = 0;
	virtual int OfflineIcon() = 0;
	virtual int BlockedIcon() = 0;
};

