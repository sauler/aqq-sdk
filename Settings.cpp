/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of SDK for AQQ IM                                         *
 *                                                                             *
 * SDK for AQQ IM is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * SDK is distributed in the hope that it will be useful,                      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#include "Settings.h"
#include "AQQ.h"
#include "Paths.h"
#include "Plugin.h"

CSettings::CSettings(UnicodeString Filename)
{
	UnicodeString Directory = CPaths::instance()->PluginUserDir() + "\\" + CPlugin::instance()->GetPluginName() + "\\Settings";
	if (!DirectoryExists(Directory))
		AQQ::Paths::ForceDirectories(Directory);
	UnicodeString Path = Directory + "\\" + Filename;
	SettingsFile = new TIniFile(Path);
}

CSettings::~CSettings()
{
	delete SettingsFile;
}

void CSettings::AddString(UnicodeString Section, UnicodeString SettingName, UnicodeString SettingValue)
{
	SettingsFile->WriteString(Section, SettingName, SettingValue);
	SettingsFile->UpdateFile();
}

void CSettings::AddInt(UnicodeString Section, UnicodeString SettingName, int SettingValue)
{
	SettingsFile->WriteInteger(Section, SettingName, SettingValue);
	SettingsFile->UpdateFile();
}

void CSettings::AddFloat(UnicodeString Section, UnicodeString SettingName, float SettingValue)
{
	SettingsFile->WriteInteger(Section, SettingName, SettingValue);
	SettingsFile->UpdateFile();
}

void CSettings::AddBool(UnicodeString Section, UnicodeString SettingName, bool SettingValue)
{
	SettingsFile->WriteInteger(Section, SettingName, SettingValue);
	SettingsFile->UpdateFile();
}

UnicodeString CSettings::GetString(UnicodeString Section, UnicodeString SettingName)
{
	return SettingsFile->ReadString(Section, SettingName, "Empty");
}

int CSettings::GetInt(UnicodeString Section, UnicodeString SettingName)
{
	return SettingsFile->ReadInteger(Section, SettingName, -1);
}

float CSettings::GetFloat(UnicodeString Section, UnicodeString SettingName)
{
	return SettingsFile->ReadFloat(Section, SettingName, -1);
}

bool CSettings::GetBool(UnicodeString Section, UnicodeString SettingName)
{
	return SettingsFile->ReadBool(Section, SettingName, false);
}

void CSettings::DeleteSetting(UnicodeString Section, UnicodeString SettingName)
{
	SettingsFile->DeleteKey(Section, SettingName);
	SettingsFile->UpdateFile();
}

bool CSettings::SectionExists(UnicodeString Section)
{
	return SettingsFile->SectionExists(Section);
}

void CSettings::EraseSection(UnicodeString Section)
{
	SettingsFile->EraseSection(Section);
	SettingsFile->UpdateFile();
}

void CSettings::ReadSections(TStringList* Sections)
{
	SettingsFile->ReadSections(Sections);
}
