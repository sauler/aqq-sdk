/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of SDK for AQQ IM                                         *
 *                                                                             *
 * SDK for AQQ IM is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * SDK is distributed in the hope that it will be useful,                      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#include "PopupMenuItem.h"
#include "AQQ.h"


CPopupMenuItem::CPopupMenuItem()
{
	this->Action = "";
	this->Name = "";
	this->Caption = "";
	this->Position = 0;
	this->IconID = 0;
	this->Service = "";
	this->PopupName = "";
	this->PopupPosition = 0;
	this->GroupIndex = 0;
	this->Grouped = false;
	this->AutoCheck = false;
	this->Checked = false;
	this->Handle = 0;
	this->Shortcut = "";
	this->Hint = "";
	this->PositionAfter = "";

	PluginAction = new TPluginAction;
	PluginActionEdit = new TPluginActionEdit;
}

CPopupMenuItem::CPopupMenuItem(UnicodeString Name, UnicodeString Caption, int Position, int IconID, UnicodeString Service, UnicodeString PopupName)
{
	PluginAction = new TPluginAction;
	PluginActionEdit = new TPluginActionEdit;

	this->Action = "";
	this->Name = Name;
	this->Caption = Caption;
	this->Position = Position;
	this->IconID = IconID;
	this->Service = Service;
	this->PopupName = PopupName;
	this->PopupPosition = 0;
	this->GroupIndex = 0;
	this->Grouped = false;
	this->AutoCheck = false;
	this->Checked = false;
	this->Handle = 0;
	this->Shortcut = "";
	this->Hint = "";
	this->PositionAfter = "";
}

CPopupMenuItem::CPopupMenuItem(UnicodeString Name, UnicodeString Caption, UnicodeString PositionAfter, int IconID, UnicodeString Service, UnicodeString PopupName)
{
	PluginAction = new TPluginAction;
    PluginActionEdit = new TPluginActionEdit;

	this->Action = "";
	this->Name = Name;
	this->Caption = Caption;
	this->Position = 0;
	this->IconID = IconID;
	this->Service = Service;
	this->PopupName = PopupName;
	this->PopupPosition = 0;
	this->GroupIndex = 0;
	this->Grouped = false;
	this->AutoCheck = false;
	this->Checked = false;
	this->Handle = 0;
	this->Shortcut = "";
	this->Hint = "";
	this->PositionAfter = PositionAfter;
}


CPopupMenuItem::~CPopupMenuItem()
{
	this->Destroy();
	delete PluginAction;
}

PPluginAction CPopupMenuItem::AQQFormat()
{
	ZeroMemory(this->PluginAction, sizeof(TPluginAction));
	this->PluginAction->cbSize = sizeof(TPluginAction);
	this->PluginAction->Action = this->Action.c_str();
	this->PluginAction->pszName = this->Name.c_str();
	this->PluginAction->pszCaption = this->Caption.c_str();
	this->PluginAction->Position = this->Position;
	this->PluginAction->IconIndex = this->IconID;
	this->PluginAction->pszService = this->Service.c_str();
	this->PluginAction->pszPopupName = this->PopupName.c_str();
	this->PluginAction->PopupPosition = this->PopupPosition;
	this->PluginAction->GroupIndex = this->GroupIndex;
	this->PluginAction->Grouped = this->Grouped;
	this->PluginAction->AutoCheck = this->AutoCheck;
	this->PluginAction->Checked = this->Checked;
	this->PluginAction->Handle = this->Handle;
	this->PluginAction->ShortCut = this->Shortcut.c_str();
	this->PluginAction->Hint = this->Hint.c_str();
	this->PluginAction->PositionAfter = this->PositionAfter.c_str();

	return this->PluginAction;
}

void CPopupMenuItem::SDKFormat(PPluginAction PluginAction)
{
	this->Action = PluginAction->Action;
	this->Name = PluginAction->pszName;
	this->Caption = PluginAction->pszCaption;
	this->Position = PluginAction->Position;
	this->IconID = PluginAction->IconIndex;
	this->Service = PluginAction->pszService;
	this->PopupName = PluginAction->pszPopupName;
	this->PopupPosition = PluginAction->PopupPosition;
	this->GroupIndex = PluginAction->GroupIndex;
	this->Grouped = PluginAction->Grouped;
	this->AutoCheck = PluginAction->AutoCheck;
	this->Checked = PluginAction->Checked;
	this->Handle = PluginAction->Handle;
	this->Shortcut = PluginAction->ShortCut;
	this->Hint = PluginAction->Hint;
	this->PositionAfter = PluginAction->PositionAfter;
}

int CPopupMenuItem::Create()
{
	return AQQ::Controls::CreatePopupMenuItem(this->AQQFormat());
}

void CPopupMenuItem::Destroy()
{
	AQQ::Controls::DestroyPopupMenuItem(this->AQQFormat());
}

void CPopupMenuItem::Get(PPluginItemDescriber PluginItemDescriber)
{
	this->PluginAction = AQQ::Controls::GetPopupMenuItem(PluginItemDescriber);
}

void CPopupMenuItem::Update()
{
	ZeroMemory(this->PluginActionEdit, sizeof(TPluginActionEdit));
	this->PluginActionEdit->cbSize = sizeof(TPluginActionEdit);
	this->PluginActionEdit->pszName = this->Name.c_str();
	this->PluginActionEdit->Caption = this->Caption.c_str();
	this->PluginActionEdit->Hint = this->Hint.c_str();
	this->PluginActionEdit->Enabled = this->Enabled;
	this->PluginActionEdit->Visible = this->Visible;
	this->PluginActionEdit->IconIndex = this->IconID;
	this->PluginActionEdit->Checked = this->Checked;
	AQQ::Controls::EditPopupMenuItem(this->PluginActionEdit);
}

