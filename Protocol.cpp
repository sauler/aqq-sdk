/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of SDK for AQQ IM                                         *
 *                                                                             *
 * SDK for AQQ IM is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * SDK is distributed in the hope that it will be useful,                      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#include "Protocol.h"

#include "AQQ.h"
#include "PluginLink.h"

CProtocol* CProtocol::Instance = NULL;

CProtocol::CProtocol()
{
	this->JID = "";
	this->Name = "";
	this->Prompt = "";
	this->Transport = false;
	this->Search = false;
	this->GroupChat = false;
	this->Description = "";
	this->RequiredID = false;
	this->IconID = -1;
	this->AccountName = "";

	CProtocol::Instance = this;
	PluginAgent = new TPluginAgent;
	CPluginLink::instance()->GetLink().HookEvent(AQQ_SYSTEM_MAXMSGLENGTH,
		MaxMessageLengthHook);
	CPluginLink::instance()->GetLink().HookEvent(AQQ_SYSTEM_MAXSTATUSLENGTH,
		MaxStatusLengthHook);
	CPluginLink::instance()->GetLink().HookEvent(AQQ_CONTACTS_ICONSHOWTYPE_PATH,
		ShowTypePathHook);
	CPluginLink::instance()->GetLink().HookEvent(AQQ_SYSTEM_CLIPBOARD_JID,
		ClipboardHook);
}

CProtocol::~CProtocol()
{
	delete PluginAgent;
	CPluginLink::instance()->GetLink().UnhookEvent(MaxMessageLengthHook);
	CPluginLink::instance()->GetLink().UnhookEvent(MaxStatusLengthHook);
	CPluginLink::instance()->GetLink().UnhookEvent(ShowTypePathHook);
	CPluginLink::instance()->GetLink().UnhookEvent(ClipboardHook);
}

PPluginAgent CProtocol::AQQFormat()
{
	ZeroMemory(this->PluginAgent,sizeof(TPluginAgent));
	this->PluginAgent->cbSize = sizeof(TPluginAgent);
	this->PluginAgent->JID = this->JID.c_str();
	this->PluginAgent->Name = this->Name.c_str();
	this->PluginAgent->Prompt = this->Prompt.c_str();
	this->PluginAgent->Transport = this->Transport;
	this->PluginAgent->Search = this->Search;
	this->PluginAgent->GroupChat = this->GroupChat;
	this->PluginAgent->Agents = false;
	this->PluginAgent->Service = L"";
	this->PluginAgent->CanRegister = false;
	this->PluginAgent->Description = this->Description.c_str();
	this->PluginAgent->RequiredID = this->RequiredID;
	this->PluginAgent->IconIndex = this->IconID;
	this->PluginAgent->PluginAccountName = this->AccountName.c_str();
	return PluginAgent;
}

void CProtocol::SDKFormat(PPluginAgent PluginAgent)
{
	this->JID = PluginAgent->JID;
	this->Name = PluginAgent->Name;
	this->Prompt = PluginAgent->Prompt;
	this->Transport = PluginAgent->Transport;
	this->Search = PluginAgent->Search;
	this->GroupChat = PluginAgent->GroupChat;
	this->Description = PluginAgent->Description;
	this->RequiredID = PluginAgent->RequiredID;
	this->IconID = PluginAgent->IconIndex;
	this->AccountName = PluginAgent->PluginAccountName;
}

int CProtocol::Register()
{
	return AQQ::System::SetAgent(this->AQQFormat());
}

int CProtocol::Update()
{
	return AQQ::System::SetAgent(this->AQQFormat());
}

bool CProtocol::Remove()
{
	if (AQQ::System::RemoveAgent(this->JID) == 0)
		return false;
	else
		return true;
}

INT_PTR __stdcall CProtocol::MaxMessageLengthHook(WPARAM wParam,LPARAM lParam)
{
	PPluginContact Contact = (PPluginContact)lParam;
	if (CProtocol::Instance->CheckContact(Contact->JID))
		return CProtocol::Instance->MaxMessageLength();
	else
		return 0;
}

INT_PTR __stdcall CProtocol::MaxStatusLengthHook(WPARAM wParam,LPARAM lParam)
{
	CProtocol::Instance->PluginMaxStatus.cbSize = sizeof(TPluginMaxStatus);
	CProtocol::Instance->PluginMaxStatus.IconIndex = CProtocol::Instance->OnlineIcon();
	CProtocol::Instance->PluginMaxStatus.Name = CProtocol::Instance->AccountName.c_str();
	CProtocol::Instance->PluginMaxStatus.Max = CProtocol::Instance->MaxStatusLength();
	return (int)&CProtocol::Instance->PluginMaxStatus;
}

INT_PTR __stdcall CProtocol::ShowTypePathHook(WPARAM wParam,LPARAM lParam)
{
	PPluginContact Contact = (PPluginContact)lParam;
	if (CProtocol::Instance->CheckContact(Contact->JID))
	{
		switch (Contact->State)
		{
		case CONTACT_OFFLINE:
			return (INT_PTR)CProtocol::Instance->OfflineIconPath().c_str();
		case CONTACT_ONLINE:
			return (INT_PTR)CProtocol::Instance->OnlineIconPath().c_str();
		case CONTACT_FFC:
			return (INT_PTR)CProtocol::Instance->FFCIconPath().c_str();
		case CONTACT_AWAY:
			return (INT_PTR)CProtocol::Instance->AwayIconPath().c_str();
		case CONTACT_NA:
			return (INT_PTR)CProtocol::Instance->NAIconPath().c_str();
		case CONTACT_DND:
			return (INT_PTR)CProtocol::Instance->DNDIconPath().c_str();
		case CONTACT_INV:
			return (INT_PTR)CProtocol::Instance->InvisibleIconPath().c_str();
		default:
			return (INT_PTR)CProtocol::Instance->OfflineIconPath().c_str();
		}
	}
	else
		return 0;
}

INT_PTR __stdcall CProtocol::ClipboardHook(WPARAM wParam, LPARAM lParam)
{
	PPluginContact Contact = (PPluginContact)lParam;
	if (CProtocol::Instance->CheckContact(Contact->JID))
		return (INT_PTR)CProtocol::Instance->Clipboard(Contact->JID).c_str();
	else
		return 0;
}
