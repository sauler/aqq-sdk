/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of SDK for AQQ IM                                         *
 *                                                                             *
 * SDK for AQQ IM is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * SDK is distributed in the hope that it will be useful,                      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#pragma once
#include <map>
#include <vcl.h>
#include "Account.h"

class CAccountManager
{
private:
	typedef std::map<int, CAccount*>::iterator Accounts_it;
	std::map<int, CAccount*> Accounts;
	int LastAddedAccount;

	// AQQ Hook
	static INT_PTR __stdcall AccountEvent(WPARAM wParam, LPARAM lParam);
	static CAccountManager* Instance;
public:
	CAccountManager();
	~CAccountManager();

	void AddAccount(CAccount *Account);
	void RemoveAccount(int AccountID);
	CAccount *GetAccountByID(int AccountID);
	CAccount *GetAccountByJID(UnicodeString JID);
	CAccount *GetAccountByName(UnicodeString Name);
	CAccount *GetLastAddedAccount();
	void GetAccounts(TStrings* Names, bool IncludeFirstAccount);
	int GetAccountsCount();
	void DeleteAllAccounts();
};
