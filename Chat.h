/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of SDK for AQQ IM                                         *
 *                                                                             *
 * SDK for AQQ IM is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * SDK is distributed in the hope that it will be useful,                      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#pragma once
#include <vcl.h>

#include "PluginAPI.h"

class CChat
{
private:
	PPluginChatOpen PluginChatOpen;
public:
	CChat();
	~CChat();

	PPluginChatOpen AQQFormat();
	void SDKFormat(PPluginChatOpen PluginChatOpen);

	void Open();

	UnicodeString JID;
	int UserIDx;
	UnicodeString ChannelName;
	UnicodeString Nick;
	bool NewWindow;
	bool IsNewMsg;
	bool Priority;
	UnicodeString OriginJID;
	int ImageIndex;
	bool AutoAccept;
	unsigned char ChatMode;
};
