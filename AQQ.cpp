/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of SDK for AQQ IM                                         *
 *                                                                             *
 * SDK for AQQ IM is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * SDK is distributed in the hope that it will be useful,                      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#include "AQQ.h"
#include "PluginLink.h"
#include <vcl.h>


int AQQ::System::AccountEvents(int ID, PPluginAccountEvents PluginAccountEvents)
{
	return CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_ACCOUNT_EVENTS, ID, (LPARAM)PluginAccountEvents);
}

void AQQ::System::AddImpExp(PPluginImpExp PluginImpExp)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_ADDIMPEXP, 0, (LPARAM)PluginImpExp);
}

int AQQ::System::AddSMSGate(PPluginSMSGate PluginSMSGate)
{
	return CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_ADDSMSGATE, 0, (LPARAM)PluginSMSGate);
}

UnicodeString AQQ::System::AppVer()
{
	return (wchar_t*)CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_APPVER, 0, 0);
}

void AQQ::System::ChangeJabberResources(int Flag, UnicodeString NewResourceName)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_CHANGE_JABBERRESOURCES, Flag, (LPARAM)NewResourceName.c_str());
}

void AQQ::System::ChangeSMSID(UnicodeString OldID, UnicodeString NewID)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_CHANGESMS_ID, (WPARAM)OldID.c_str(), (LPARAM)NewID.c_str());
}

void AQQ::System::ChangeSMSStatus(int SMSStateConst, UnicodeString  SMSID)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_CHANGESMS_STATUS, SMSStateConst, (LPARAM)SMSID.c_str());
}

void AQQ::System::Chat(PPluginChatPrep PluginChatPrep)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_CHAT, 0, (LPARAM)PluginChatPrep);
}

void AQQ::System::ChatOpen(PPluginChatOpen PluginChatOpen)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_CHAT_OPEN, 0, (LPARAM)PluginChatOpen);
}

void AQQ::System::ChatPresence(PPluginChatPresence PluginChatPresence)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_CHAT_PRESENCE, 0, (LPARAM)PluginChatPresence);
}

void AQQ::System::ColorChange(int Hue, int Saturation, int Brightness)
{
	PPluginColorChange ColorChange;
	ColorChange = new TPluginColorChange;
	ColorChange->cbSize = sizeof(TPluginColorChange);
	ColorChange->Hue = Hue;
	ColorChange->Saturation = Saturation;
	ColorChange->Brightness = Brightness;
	CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_COLORCHANGEV2, (WPARAM)ColorChange, 0);
	delete ColorChange;
}

int AQQ::System::GetBrightness()
{
	return CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_COLORGETBRIGHTNESS, 0, 0);
}

int AQQ::System::GetHue()
{
	return CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_COLORGETHUE, 0, 0);
}

int AQQ::System::GetSaturation()
{
	return CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_COLORGETSATURATION, 0, 0);
}

void AQQ::System::DebugXML(PPluginDebugInfo PluginDebugInfo)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_DEBUG_XML, 0, (LPARAM)PluginDebugInfo);
}

int AQQ::System::Error(PPluginError PluginError)
{
	return CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_ERROR, 0, (LPARAM)PluginError);
}

void AQQ::System::SetEnabled(int FunctionID, int Enable)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_FUNCTION_SETENABLED, FunctionID, Enable);
}

UnicodeString  AQQ::System::GetNewCacheItemPatch(UnicodeString Filename, UnicodeString ContactJID)
{
	return (wchar_t*)CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_GETNEWCACHEITEMPATH, (WPARAM)Filename.c_str(), (LPARAM)ContactJID.c_str());
}

void AQQ::System::InterpretXML(int AccountID, UnicodeString XML)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_INTERPRET_XML, AccountID, (LPARAM)XML.c_str());
}

bool AQQ::System::ModulesLoaded()
{
	if (CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_MODULESLOADED, 0, 0) == 0)
		return false;
	else
		return true;
}

int AQQ::System::MsgComposing(PPluginContact PluginContact, PPluginChatState PluginChatState)
{
	return CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_MSGCOMPOSING, (WPARAM)PluginContact, (LPARAM)PluginChatState);
}

void AQQ::System::NewsSourceAdd(PPluginNewsData PluginNewsData, int AccountID)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_NEWSSOURCE_ADD, (WPARAM)PluginNewsData, AccountID);
}

int AQQ::System::NewsSourceDelete(PPluginNewsData PluginNewsData)
{
	return CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_NEWSSOURCE_DELETE, (WPARAM)PluginNewsData, 0);
}

int AQQ::System::NewsSourceFetchEnd(int ID, int State)
{
	return CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_NEWSSOURCE_FETCHEND, ID, State);
}

void AQQ::System::NewsSourceFetchStart(int ID)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_NEWSSOURCE_FETCHSTART, ID, 0);
}

void AQQ::System::NewsSourceItem(PPluginNewsItem PluginNewsItem)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_NEWSSOURCE_ITEM, (WPARAM)PluginNewsItem, 0);
}

int AQQ::System::NewsSourceRefresh(PPluginNewsData PluginNewsData)
{
	return CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_NEWSSOURCE_REFRESH, (WPARAM)PluginNewsData, 0);
}

int AQQ::System::NewsSourceUpdate(PPluginNewsData PluginNewsData)
{
	return CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_NEWSSOURCE_UPDATE, (WPARAM)PluginNewsData, 0);
}

void AQQ::System::OnConnectSilence()
{
	CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_ONCONNECT_SILENCE, 0, 0);
}

void AQQ::System::PlaySound(int SoundID, int Flag)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_PLAYSOUND, SoundID, Flag);
}

void AQQ::System::PlaySound(UnicodeString Filename, int Flag)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_PLAYSOUND, (WPARAM)Filename.c_str(), Flag);
}

int AQQ::System::PluginActive(int Mode, UnicodeString Filename)
{
	return CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_PLUGIN_ACTIVE, Mode, (LPARAM)Filename.c_str());
}

int AQQ::System::PluginExclude(int Mode, UnicodeString Filename)
{
	return CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_PLUGIN_EXCLUDE, Mode, (LPARAM)Filename.c_str());
}

void AQQ::System::RefreshPlugins(int Mode)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_PLUGIN_REFRESHLIST, 0, Mode);
}

void AQQ::System::RefreshPlugin(UnicodeString Filename)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_PLUGIN_REFRESHLIST, (WPARAM)Filename.c_str(), 0);
}

int AQQ::System::RegExp(UnicodeString Regexp, int Mode)
{
	return CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_REGEXP, (WPARAM)Regexp.c_str(), Mode);
}

int AQQ::System::RemoveAgent(UnicodeString JID)
{
	return CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_REMOVEAGENT, (WPARAM)JID.c_str(), 0);
}

int AQQ::System::RemoveSMSGate(int ID)
{
	return CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_REMOVESMSGATE, ID, 0);
}

int AQQ::System::RenameSMSGate(UnicodeString NewName, int ID)
{
	return CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_RENAMESMSGATE, (WPARAM)NewName.c_str(), ID);
}

void AQQ::System::Restart(int Mode)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_RESTART, Mode, 0);
}

int AQQ::System::RunAction(UnicodeString Action)
{
	return CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_RUNACTION, 0, (LPARAM)Action.c_str());
}

void AQQ::System::SendHook(PPluginHook PluginHook)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_SENDHOOK, (WPARAM)PluginHook, 0);
}

int AQQ::System::SendXML(UnicodeString XML, int AccountID)
{
	return CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_SENDXML, (WPARAM)XML.c_str(), AccountID);
}

int AQQ::System::SetAgent(PPluginAgent PluginAgent)
{
	return CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_SETAGENT, 0, (LPARAM)PluginAgent);
}

void AQQ::System::SetShowAndStatus(PPluginStateChange PluginStateChange)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_SETSHOWANDSTATUS, 0, (LPARAM)PluginStateChange);
}

int AQQ::System::SocialSend(int SocialMedia, UnicodeString Message)
{
	return CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_SOCIALSEND, SocialMedia, (LPARAM)Message.c_str());
}

void AQQ::System::ApplyTheme(int Mode)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_THEME_APPLY, 0, Mode);
}

void AQQ::System::RefreshTheme()
{
	CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_THEME_REFRESH, 0, 0);
}

void AQQ::System::SetTheme(UnicodeString Path)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_THEME_SET, 0, (LPARAM)Path.c_str());
}

void AQQ::System::TooltipAddItem(PPluginToolTipItem PluginToolTipItem)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_TOOLTIP_ADDITEM, 0, (LPARAM)PluginToolTipItem);
}

void AQQ::System::TooltipShow(int Delay)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_TOOLTIP_SHOW, Delay, 0);
}

void AQQ::System::ChangeTransferStatus(PPluginTransfer PluginTransfer)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_TRANSFER_STATUS_CHANGE, 0, (LPARAM)PluginTransfer);
}

void AQQ::System::RefreshTrayIcon()
{
	CPluginLink::instance()->GetLink().CallService(AQQ_SYSTEM_TRAYICONREFRESH, 0, 0);
}

void AQQ::Controls::CreateButton(PPluginAction PluginAction)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_CONTROLS_TOOLBAR "ToolDown" AQQ_CONTROLS_CREATEBUTTON, 0, (LPARAM)PluginAction);
}

void AQQ::Controls::CreatePopupMenu(PPluginAction PluginAction)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_CONTROLS_CREATEPOPUPMENU, 0, (LPARAM)PluginAction);
}

int AQQ::Controls::CreatePopupMenuItem(PPluginAction PluginAction)
{
	return CPluginLink::instance()->GetLink().CallService(AQQ_CONTROLS_CREATEPOPUPMENUITEM, 0, (LPARAM)PluginAction);
}

void AQQ::Controls::DestroyButton(PPluginAction PluginAction)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_CONTROLS_TOOLBAR "ToolDown" AQQ_CONTROLS_DESTROYBUTTON, 0, (LPARAM)PluginAction);
}

void AQQ::Controls::DestroyPopupMenu(PPluginAction PluginAction)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_CONTROLS_DESTROYPOPUPMENU, 0, (LPARAM)PluginAction);
}

void AQQ::Controls::DestroyPopupMenuItem(PPluginAction PluginAction)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_CONTROLS_DESTROYPOPUPMENUITEM, 0, (LPARAM)PluginAction);
}

void AQQ::Controls::EditPopupMenuItem(PPluginActionEdit PluginActionEdit)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_CONTROLS_EDITPOPUPMENUITEM, 0, (LPARAM)PluginActionEdit);
}

PPluginAction AQQ::Controls::GetPopupMenuItem(PPluginItemDescriber PluginItemDescriber)
{
	return PPluginAction(CPluginLink::instance()->GetLink().CallService(AQQ_CONTROLS_GETPOPUPMENUITEM, 0, (LPARAM)PluginItemDescriber));
}

void AQQ::Controls::SetMainStatusPanelText(UnicodeString NewStatus)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_CONTROLS_MAINSTATUS_SETPANELTEXT, 0, (LPARAM)NewStatus.c_str());
}

void AQQ::Controls::SMSControlsEnable(int Enable)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_CONTROLS_SMSCONTROLS_ENABLE, 0, Enable);
}

void AQQ::Controls::UpdateButton(PPluginAction PluginAction)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_CONTROLS_TOOLBAR "ToolDown" AQQ_CONTROLS_UPDATEBUTTON, 0, (LPARAM)PluginAction);
}

int AQQ::Controls::WebBrowserClickID(PPluginWebItem PluginWebItem, PPluginWebBrowser PluginWebBrowser)
{
	return CPluginLink::instance()->GetLink().CallService(AQQ_CONTROLS_WEBBROWSER_CLICKID, (WPARAM)PluginWebItem, (LPARAM)PluginWebBrowser);
}

int AQQ::Controls::WebBrowserCreate(int handle, PPluginWebBrowser PluginWebBrowser)
{
	return CPluginLink::instance()->GetLink().CallService(AQQ_CONTROLS_WEBBROWSER_CREATE, handle, (LPARAM)PluginWebBrowser);
}

int AQQ::Controls::WebBrowserDestroy(PPluginWebBrowser PluginWebBrowser)
{
	return CPluginLink::instance()->GetLink().CallService(AQQ_CONTROLS_WEBBROWSER_DESTROY, 0, (LPARAM)PluginWebBrowser);
}

void AQQ::Controls::WebBrowserGetID(PPluginWebItem PluginWebItem, PPluginWebBrowser PluginWebBrowser)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_CONTROLS_WEBBROWSER_GETID, (WPARAM)PluginWebItem, (LPARAM)PluginWebBrowser);
}

int AQQ::Controls::WebBrowserNavigate(UnicodeString URL, PPluginWebBrowser PluginWebBrowser)
{
	return CPluginLink::instance()->GetLink().CallService(AQQ_CONTROLS_WEBBROWSER_NAVIGATE, (WPARAM)URL.c_str(), (LPARAM)PluginWebBrowser);
}

int AQQ::Controls::WebBrowserSetID(PPluginWebItem PluginWebItem, PPluginWebBrowser PluginWebBrowser)
{
	return CPluginLink::instance()->GetLink().CallService(AQQ_CONTROLS_WEBBROWSER_NAVIGATE, (WPARAM)PluginWebItem, (LPARAM)PluginWebBrowser);
}

void AQQ::Contacts::AckMsg(int AckID, int Status)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_CONTACTS_ACKMSG, AckID, Status);
}

void AQQ::Contacts::AddBan(UnicodeString ContactJID)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_CONTACTS_ADD_BAN, 0, (LPARAM)ContactJID.c_str());
}

void AQQ::Contacts::AddForm(PPluginAddForm PluginAddForm)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_CONTACTS_ADDFORM, 0, (LPARAM)PluginAddForm);
}

int AQQ::Contacts::AddLineInfo(PPluginContact Contact, UnicodeString Message, bool SaveToArchive)
{
	TPluginMicroMsg PluginMicroMsg;
	PluginMicroMsg.cbSize = sizeof(TPluginMicroMsg);
	PluginMicroMsg.Msg = Message.c_str();
	PluginMicroMsg.SaveToArchive = SaveToArchive;
	return CPluginLink::instance()->GetLink().CallService(AQQ_CONTACTS_ADDLINEINFO, (WPARAM)Contact, (LPARAM)&PluginMicroMsg);
}

void AQQ::Contacts::CloseTab(int Mode, UnicodeString ContactJID)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_CONTACTS_BUDDY_CLOSETAB, Mode, (LPARAM)ContactJID.c_str());
}

void AQQ::Contacts::FetchAllTabs()
{
	CPluginLink::instance()->GetLink().CallService(AQQ_CONTACTS_BUDDY_FETCHALLTABS, 0, 0);
}

void AQQ::Contacts::SetTabCaption(UnicodeString Name, PPluginContact Contact)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_CONTACTS_BUDDY_TABCAPTION, (WPARAM)Name.c_str(), (LPARAM)Contact);
}

void AQQ::Contacts::SetTabIcon(int iconID, PPluginContact Contact)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_CONTACTS_BUDDY_TABIMAGE, iconID, (LPARAM)Contact);
}

void AQQ::Contacts::Create(PPluginContact Contact)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_CONTACTS_CREATE, 0, (LPARAM)Contact);
}

void AQQ::Contacts::Delete(PPluginContact Contact)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_CONTACTS_DELETE, 0, (LPARAM)Contact);
}

void AQQ::Contacts::DestroyDomain(UnicodeString Domain)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_CONTACTS_DESTROYDOMAIN, 0, (LPARAM)Domain.c_str());
}

void AQQ::Contacts::SearchXMLError(UnicodeString XML)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_CONTACTS_ERRSEARCHXML, (WPARAM)XML.c_str(), 0);
}

int AQQ::Contacts::FillSimpleInfo(PPluginContactSimpleInfo PluginContactSimpleInfo)
{
	return CPluginLink::instance()->GetLink().CallService(AQQ_CONTACTS_FILLSIMPLEINFO, 0, (LPARAM)PluginContactSimpleInfo);
}

UnicodeString AQQ::Contacts::GetHTMLStatus(PPluginContact PluginContact)
{
	return UnicodeString(CPluginLink::instance()->GetLink().CallService(AQQ_CONTACTS_GETHTMLSTATUS, (WPARAM)PluginContact, 0));
}

bool AQQ::Contacts::HaveBan(UnicodeString ContactJID)
{
	if (CPluginLink::instance()->GetLink().CallService(AQQ_CONTACTS_HAVE_BAN, 0, (LPARAM)ContactJID.c_str()) == 0)
		return false;
	else
		return true;
}

void AQQ::Contacts::Message(PPluginMessage PluginMessage)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_CONTACTS_MESSAGE, 0, (LPARAM)PluginMessage);
}

void AQQ::Contacts::RemoveBan(UnicodeString ContactJID)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_CONTACTS_REMOVE_BAN, 0, (LPARAM)ContactJID.c_str());
}

int AQQ::Contacts::RequestBuddy(int AccountID, UnicodeString ContactJID)
{
	return CPluginLink::instance()->GetLink().CallService(AQQ_CONTACTS_REQUESTBUDDY, AccountID, (LPARAM)ContactJID.c_str());
}

void AQQ::Contacts::RequestList()
{
	CPluginLink::instance()->GetLink().CallService(AQQ_CONTACTS_REQUESTLIST, GetTickCount(), 0);
}

void AQQ::Contacts::SearchXMLResult(UnicodeString XML)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_CONTACTS_RESSEARCHXML, (WPARAM)XML.c_str(), 0);
}

void AQQ::Contacts::VCardResult(UnicodeString SessionID, UnicodeString XML)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_CONTACTS_RESVCARD, (WPARAM)SessionID.c_str(), (LPARAM)XML.c_str());
}

bool AQQ::Contacts::SendMessage(PPluginContact PluginContact, PPluginMessage PluginMessage)
{
	if (CPluginLink::instance()->GetLink().CallService(AQQ_CONTACTS_SENDMSG, (WPARAM)PluginContact, (LPARAM)PluginMessage) == 0)
		return false;
	else
		return true;
}

bool AQQ::Contacts::SendImage(PPluginContact PluginContact, UnicodeString Filename)
{
	if (CPluginLink::instance()->GetLink().CallService(AQQ_CONTACTS_SENDPIC, (WPARAM)PluginContact, (LPARAM)Filename.c_str()) == 0)
		return false;
	else
		return true;
}

bool AQQ::Contacts::SetAvatar(PPluginAvatar PluginAvatar, PPluginContact PluginContact)
{
	if (CPluginLink::instance()->GetLink().CallService(AQQ_CONTACTS_SET_AVATAR, (WPARAM)PluginAvatar, (LPARAM)PluginContact) == 0)
		return false;
	else
		return true;
}

void AQQ::Contacts::SetSimpleInfo(PPluginContactSimpleInfo PluginContactSimpleInfo)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_CONTACTS_SET_SIMPLEINFO, 0, (LPARAM)PluginContactSimpleInfo);
}

void AQQ::Contacts::SetHTMLStatus(PPluginContact PluginContact, UnicodeString HTMLStatus)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_CONTACTS_SETHTMLSTATUS, (WPARAM)PluginContact, (LPARAM)HTMLStatus.c_str());
}

void AQQ::Contacts::SetOffline(UnicodeString Domain)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_CONTACTS_SETOFFLINE, 0, (LPARAM)Domain.c_str());
}

void AQQ::Contacts::SetWebAvatar(UnicodeString URL, PPluginContact PluginContact)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_CONTACTS_SETWEB_AVATAR, (WPARAM)URL.c_str(), (LPARAM)PluginContact);
}

void AQQ::Contacts::Update(PPluginContact PluginContact)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_CONTACTS_UPDATE, 0, (LPARAM)PluginContact);
}

UnicodeString AQQ::Contacts::GetStatePNGFilePath(PPluginContact PluginContact)
{
	return UnicodeString(CPluginLink::instance()->GetLink().CallService(AQQ_FUNCTION_GETSTATEPNG_FILEPATH, 0, (LPARAM)PluginContact));
}

int AQQ::Contacts::GetStatePNGIndex(PPluginContact PluginContact)
{
	return CPluginLink::instance()->GetLink().CallService(AQQ_FUNCTION_GETSTATEPNG_INDEX, 0, (LPARAM)PluginContact);
}

void AQQ::Icons::DestroyIcon(int ID)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_ICONS_DESTROYPNGICON, 0, ID);
}

int AQQ::Icons::LoadIcon(UnicodeString Filename)
{
	return CPluginLink::instance()->GetLink().CallService(AQQ_ICONS_LOADPNGICON, 0, (LPARAM)Filename.c_str());
}

int AQQ::Icons::ReplaceIcon(int IconID, UnicodeString Filename)
{
	return CPluginLink::instance()->GetLink().CallService(AQQ_ICONS_LOADPNGICON, IconID, (LPARAM)Filename.c_str());
}

UnicodeString AQQ::Icons::GetPNGHDFilePath(int IconID)
{
	return (wchar_t*)CPluginLink::instance()->GetLink().CallService(AQQ_FUNCTION_GETPNGHD_FILEPATH, 0, IconID);
}

UnicodeString AQQ::Icons::GetPathFromID(int IconID)
{
	return (wchar_t*)CPluginLink::instance()->GetLink().CallService(AQQ_FUNCTION_GETPNG_FILEPATH, IconID, 0);
}

UnicodeString AQQ::Paths::GetAppFilePath()
{
	return (wchar_t*)CPluginLink::instance()->GetLink().CallService(AQQ_FUNCTION_GETAPPFILEPATH, 0, 0);
}

UnicodeString AQQ::Paths::GetAppPath()
{
	return (wchar_t*)CPluginLink::instance()->GetLink().CallService(AQQ_FUNCTION_GETAPPPATH, 0, 0);
}

UnicodeString AQQ::Paths::GetPluginDir(int Handle)
{
	return (wchar_t*)CPluginLink::instance()->GetLink().CallService(AQQ_FUNCTION_GETPLUGINDIR, Handle, 0);
}

UnicodeString AQQ::Paths::GetPluginUserDir()
{
	return (wchar_t*)CPluginLink::instance()->GetLink().CallService(AQQ_FUNCTION_GETPLUGINUSERDIR, 0, 0);
}

UnicodeString AQQ::Paths::GetThemeDir()
{
	return (wchar_t*)CPluginLink::instance()->GetLink().CallService(AQQ_FUNCTION_GETTHEMEDIR, 0, 0);
}

UnicodeString AQQ::Paths::GetThemeDirRW()
{
	return (wchar_t*)CPluginLink::instance()->GetLink().CallService(AQQ_FUNCTION_GETTHEMEDIRRW, 0, 0);
}

UnicodeString AQQ::Paths::GetUserDir()
{
	return (wchar_t*)CPluginLink::instance()->GetLink().CallService(AQQ_FUNCTION_GETUSERDIR, 0, 0);
}

void AQQ::Paths::ForceDirectories(UnicodeString Directory)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_FUNCTION_FORCEDIRECTORIES, (WPARAM)Directory.c_str(), 0);
}

void AQQ::Functions::Log(int Mode, UnicodeString Message)
{
	CPluginLink::instance()->GetLink().CallService(AQQ_FUNCTION_LOG, Mode, (LPARAM)Message.c_str());
}

PPluginProxy AQQ::Functions::GetProxy()
{
	return PPluginProxy(CPluginLink::instance()->GetLink().CallService(AQQ_FUNCTION_GETPROXY, 0, 0));
}

int AQQ::Functions::ShowMessage(int Type, UnicodeString Message)
{
	return CPluginLink::instance()->GetLink().CallService(AQQ_FUNCTION_SHOWMESSAGE, Type, (LPARAM)Message.c_str());
}

bool AQQ::Functions::IsListReady()
{
	if (CPluginLink::instance()->GetLink().CallService(AQQ_FUNCTION_ISLISTREADY, 0, 0) == 0)
		return false;
	else
		return true;
}

void AQQ::Functions::ExtractResource(UnicodeString Filename, UnicodeString ResourceName, UnicodeString ResourceType)
{
	TPluginTwoFlagParams PluginTwoFlagParams;
	PluginTwoFlagParams.cbSize = sizeof(TPluginTwoFlagParams);
	PluginTwoFlagParams.Param1 = ResourceName.c_str();
	PluginTwoFlagParams.Param2 = ResourceType.c_str();
	PluginTwoFlagParams.Flag1 = (int)HInstance;
	CPluginLink::instance()->GetLink().CallService(AQQ_FUNCTION_SAVERESOURCE,(WPARAM)&PluginTwoFlagParams,(LPARAM)Filename.c_str());
}

UnicodeString AQQ::Functions::FetchSettings()
{
	return (wchar_t*)CPluginLink::instance()->GetLink().CallService(AQQ_FUNCTION_FETCHSETUP,0,0);
}
