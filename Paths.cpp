/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of SDK for AQQ IM                                         *
 *                                                                             *
 * SDK for AQQ IM is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * SDK is distributed in the hope that it will be useful,                      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#include "Paths.h"
#include"AQQ.h"

CPaths* CPaths::Instance = 0;

CPaths* CPaths::instance()
{
	if (Instance == 0)
		Instance = new CPaths();
	return Instance;
}

UnicodeString CPaths::AppFilePath()
{
	return AQQ::Paths::GetAppFilePath();
}

UnicodeString CPaths::AppPath()
{
	return AQQ::Paths::GetAppPath();
}


UnicodeString CPaths::PluginDir(int handle)
{
	return AQQ::Paths::GetPluginDir(handle);
}


UnicodeString CPaths::PluginUserDir()
{
	return AQQ::Paths::GetPluginUserDir();
}


UnicodeString CPaths::ThemeDir()
{
	return AQQ::Paths::GetThemeDir();
}


UnicodeString CPaths::ThemeDirRW()
{
	return AQQ::Paths::GetThemeDirRW();
}


UnicodeString CPaths::UserDir()
{
	return AQQ::Paths::GetUserDir();
}

void CPaths::CreateDir(UnicodeString Directory)
{
	if (DirectoryExists(Directory))
		AQQ::Paths::ForceDirectories(Directory);
}

bool CPaths::DirectoryExists(UnicodeString Directory)
{
	return DirectoryExists(Directory);
}
