/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of SDK for AQQ IM                                         *
 *                                                                             *
 * SDK for AQQ IM is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * SDK is distributed in the hope that it will be useful,                      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#include "ContactInfo.h"
#include "Contact.h"
#include "AQQ.h"

CContactInfo::CContactInfo(CContact* Contact)
{
	this->FirstName = "";
	this->LastName = "";
	this->CellPhone = "";
	this->Phone = "";
	this->Mail = "";

	PluginContactSimpleInfo = new TPluginContactSimpleInfo;
	this->Contact = Contact;
}

CContactInfo::~CContactInfo()
{
	delete PluginContactSimpleInfo;
}

PPluginContactSimpleInfo CContactInfo::AQQFormat()
{
	ZeroMemory(this->PluginContactSimpleInfo, sizeof(TPluginContactSimpleInfo));
	this->PluginContactSimpleInfo->cbSize = sizeof(TPluginContactSimpleInfo);
	this->PluginContactSimpleInfo->JID = this->Contact->JID.c_str();
	this->PluginContactSimpleInfo->First = this->FirstName.c_str();
	this->PluginContactSimpleInfo->Last = this->LastName.c_str();
	this->PluginContactSimpleInfo->Nick = this->Contact->Nick.c_str();
	this->PluginContactSimpleInfo->CellPhone = this->CellPhone.c_str();
	this->PluginContactSimpleInfo->Phone = this->Phone.c_str();
	this->PluginContactSimpleInfo->Mail = this->Mail.c_str();
	return this->PluginContactSimpleInfo;
}

void CContactInfo::SDKFormat(PPluginContactSimpleInfo PluginContactSimpleInfo)
{
	this->FirstName = PluginContactSimpleInfo->First;
	this->LastName = PluginContactSimpleInfo->Last;
	this->CellPhone = PluginContactSimpleInfo->CellPhone;
	this->Phone = PluginContactSimpleInfo->Phone;
	this->Mail = PluginContactSimpleInfo->Mail;
}

void CContactInfo::GetSimpleInfo()
{
	AQQ::Contacts::FillSimpleInfo(this->AQQFormat());
}

void CContactInfo::SetSimpleInfo()
{
	AQQ::Contacts::SetSimpleInfo(this->AQQFormat());
}
