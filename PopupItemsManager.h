/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of SDK for AQQ IM                                         *
 *                                                                             *
 * SDK for AQQ IM is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * SDK is distributed in the hope that it will be useful,                      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#pragma once
#include <map>
#include <vcl.h>
#include "PopupMenuItem.h"

class CPopupItemsManager
{
private:
	typedef std::map<UnicodeString,CPopupMenuItem*>::iterator Items_it;
	std::map<UnicodeString,CPopupMenuItem*> Items;
	UnicodeString LastAddedItem;

public:
	CPopupItemsManager();
	~CPopupItemsManager();

	void AddItem(CPopupMenuItem* Item);
	void RemoveItem(UnicodeString Name);
	CPopupMenuItem* GetItemByName(UnicodeString Name);
	CPopupMenuItem* GetItemByCaption(UnicodeString Caption);
	CPopupMenuItem* GetLastAddedItem();
	void DestroyAllItems();
};
