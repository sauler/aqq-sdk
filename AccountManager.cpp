/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of SDK for AQQ IM                                         *
 *                                                                             *
 * SDK for AQQ IM is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * SDK is distributed in the hope that it will be useful,                      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#include "AccountManager.h"
#include "PluginLink.h"

CAccountManager* CAccountManager::Instance = NULL;

CAccountManager::CAccountManager()
{
	CAccountManager::Instance = this;
	CPluginLink::instance()->GetLink().HookEvent(AQQ_SYSTEM_ACCOUNT_RUNEVENT,
		AccountEvent);
}

CAccountManager::~CAccountManager()
{
	CPluginLink::instance()->GetLink().UnhookEvent(AccountEvent);
	CAccountManager::Instance = NULL;
	this->DeleteAllAccounts();
}

void CAccountManager::AddAccount(CAccount* Account)
{
	Accounts[Account->GetAccountID()] = Account;
	LastAddedAccount = Account->GetAccountID();
}

void CAccountManager::RemoveAccount(int AccountID)
{
	CAccount* Account = Accounts[AccountID];
	delete Account;
	Accounts.erase(AccountID);
}

CAccount* CAccountManager::GetAccountByID(int AccountID)
{
	return Accounts[AccountID];
}

CAccount* CAccountManager::GetAccountByJID(UnicodeString JID)
{
	if (JID == "")
		return NULL;

	for (Accounts_it it = Accounts.begin();it != Accounts.end();++it)
		if (it->second->GetDetails()->JID == JID)
			return it->second;

	return NULL;
}

CAccount* CAccountManager::GetAccountByName(UnicodeString Name)
{
	for (Accounts_it it = Accounts.begin();it != Accounts.end();++it)
		if (it->second->Name == Name)
			return it->second;

	return NULL;
}

CAccount* CAccountManager::GetLastAddedAccount()
{
	return GetAccountByID(LastAddedAccount);
}

void CAccountManager::GetAccounts(TStrings* Names, bool IncludeFirstAccount)
{
	for (Accounts_it it = Accounts.begin();it != Accounts.end();++it)
		if (!IncludeFirstAccount)
		{
			if (!it->second->CanDelete == false)
				Names->Add(it->second->Name);
		}
		else
			Names->Add(it->second->Name);
}

int CAccountManager::GetAccountsCount()
{
    return Accounts.size();
}

void CAccountManager::DeleteAllAccounts()
{
	for (Accounts_it it = Accounts.begin();it != Accounts.end();++it)
		RemoveAccount(it->second->GetAccountID());
}


INT_PTR __stdcall CAccountManager::AccountEvent(WPARAM wParam,LPARAM lParam)
{
	CAccount* Account = CAccountManager::Instance->GetAccountByID(wParam);
	if (Account == NULL)
		return 0;
	else
		switch (lParam)
		{
		case ACCOUNT_EVENT_DEFAULT:
			Account->OnDefaultEvent();
			break;
		case ACCOUNT_EVENT_NEW:
			Account->OnNewEvent();
			break;
		case ACCOUNT_EVENT_EDIT:
			Account->OnEditEvent();
			break;
		case ACCOUNT_EVENT_DELETE:
			Account->OnDeleteEvent();
			CAccountManager::Instance->RemoveAccount(Account->GetAccountID());
			break;
		case ACCOUNT_EVENT_CHANGEPASS:
			Account->OnChangePassEvent();
			break;
		default:;
		}
	return 0;
}
