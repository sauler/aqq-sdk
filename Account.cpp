/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of SDK for AQQ IM                                         *
 *                                                                             *
 * SDK for AQQ IM is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * SDK is distributed in the hope that it will be useful,                      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#include "Account.h"
#include "AQQ.h"
#include "IconManager.h"


CAccount::CAccount()
{
	this->Name = "";
	this->IconID = -1;
	this->CanCreate = false;
	this->CanEdit = false;
	this->CanDelete = false;
	this->CanChangePass = false;
	this->DefaultEvent = false;

	PluginAccountEvents = new TPluginAccountEvents;
	//Details = new CAccountDetails();
	Settings = new CSettings("Accounts.dat");
}

CAccount::~CAccount()
{
	delete Settings;
	//delete Details;
	delete PluginAccountEvents;
}

PPluginAccountEvents CAccount::AQQFormat()
{
	ZeroMemory(this->PluginAccountEvents, sizeof(TPluginAccountEvents));
	this->PluginAccountEvents->cbSize = sizeof(TPluginAccountEvents);
	this->PluginAccountEvents->DisplayName = this->Name.c_str();
	this->PluginAccountEvents->IconIndex = this->IconID;
	this->PluginAccountEvents->EventNew = this->CanCreate;
	this->PluginAccountEvents->EventEdit = this->CanEdit;
	this->PluginAccountEvents->EventDelete = this->CanDelete;
	this->PluginAccountEvents->EventPassChange = this->CanChangePass;
	this->PluginAccountEvents->EventDefault = this->DefaultEvent;

	return PluginAccountEvents;
}

void CAccount::SDKFormat(PPluginAccountEvents PluginAccountEvents)
{
	this->Name = PluginAccountEvents->DisplayName;
	this->IconID = PluginAccountEvents->IconIndex;
	this->CanCreate = PluginAccountEvents->EventNew;
	this->CanEdit = PluginAccountEvents->EventEdit;
	this->CanDelete = PluginAccountEvents->EventDelete;
	this->CanChangePass = PluginAccountEvents->EventPassChange;
	this->DefaultEvent = PluginAccountEvents->EventDefault;
}

void CAccount::Create()
{
	this->AccountID = AQQ::System::AccountEvents(0, this->AQQFormat());
}

void CAccount::Update()
{
	this->AccountID = AQQ::System::AccountEvents(this->AccountID, this->AQQFormat());
}

void CAccount::Delete()
{
	ZeroMemory(this->PluginAccountEvents, sizeof(TPluginAccountEvents));
	AQQ::System::AccountEvents(this->AccountID, this->PluginAccountEvents);
}

int CAccount::GetAccountID()
{
	return this->AccountID;
}

CAccountDetails *CAccount::GetDetails()
{
    return this->Details;
}

void CAccount::SaveSettings()
{
	Settings->AddString(Name, "Name", Name);
	Settings->AddString(Name, "JID", Details->JID);
	Settings->AddString(Name, "Password", Details->Password);
}

void CAccount::LoadSettings()
{
	Details->JID = Settings->GetString(Name, "JID");
	Details->Password = Settings->GetString(Name, "Password");
}

void CAccount::EraseSettings()
{
	Settings->EraseSection(Name);
}


