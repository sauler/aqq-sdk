/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of SDK for AQQ IM                                         *
 *                                                                             *
 * SDK for AQQ IM is free software: you can redistribute it and/or modify      *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * SDK is distributed in the hope that it will be useful,                      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#pragma once
#include <vcl.h>

class CTheme
{
private:
	CTheme();
	static CTheme* Instance;
	UnicodeString ActiveTheme;

	static INT_PTR __stdcall ThemeChanged(WPARAM wParam, LPARAM lParam);
	static INT_PTR __stdcall ThemeInfo(WPARAM wParam, LPARAM lParam);
	static INT_PTR __stdcall ThemeStart(WPARAM wParam, LPARAM lParam);
public:
	~CTheme();
	static CTheme* instance();
	static void InitInstance();
	static void ResetInstance();

	bool SkinEnabled();
	bool AnimateWindows();
	bool Glowing();

	void Apply(int Mode);
	void Refresh();
	void Set(UnicodeString Directory);

	void ChangeColor(int Hue, int Saturation, int Brightness);
	int Hue();
	int Saturation();
	int Brightness();
	UnicodeString GetActiveTheme();

};
